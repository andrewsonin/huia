use crate::block::BlockIdx;
use crate::clause::{Clause, ClauseIdx, Clauseable};
use crate::location::{Locatable, Location};
use crate::stable::StringIdx;
use crate::ty::{ResultType, TyIdx};

#[derive(Debug, Clone)]
pub struct Function {
    clauses: Vec<Clause>,
    location: Location,
    return_type: TyIdx,
}

impl Function {
    pub fn is_empty(&self) -> bool {
        self.clauses.is_empty()
    }

    pub fn new(return_type: TyIdx, location: Location) -> Function {
        Function {
            clauses: Vec::new(),
            location,
            return_type,
        }
    }
}

impl Clauseable for Function {
    fn clauses(&self) -> Vec<&Clause> {
        self.clauses.iter().collect()
    }

    fn has_clauses(&self) -> bool {
        !self.clauses.is_empty()
    }

    fn push_clause(
        &mut self,
        arguments: Vec<(StringIdx, TyIdx)>,
        block: BlockIdx,
        result_type: TyIdx,
    ) -> ClauseIdx {
        let idx = self.clauses.len();
        self.clauses
            .push(Clause::new(arguments, block, result_type));
        idx.into()
    }
}

impl Clauseable for &mut Function {
    fn clauses(&self) -> Vec<&Clause> {
        self.clauses.iter().collect()
    }

    fn has_clauses(&self) -> bool {
        !self.clauses.is_empty()
    }

    fn push_clause(
        &mut self,
        arguments: Vec<(StringIdx, TyIdx)>,
        block: BlockIdx,
        result_type: TyIdx,
    ) -> ClauseIdx {
        let idx = self.clauses.len();
        self.clauses
            .push(Clause::new(arguments, block, result_type));
        idx.into()
    }
}

impl Locatable for Function {
    fn location(&self) -> &Location {
        &self.location
    }
}

impl Locatable for &Function {
    fn location(&self) -> &Location {
        &self.location
    }
}

impl Locatable for &mut Function {
    fn location(&self) -> &Location {
        &self.location
    }
}

impl ResultType for Function {
    fn result_type(&self) -> &TyIdx {
        &self.return_type
    }
}

impl ResultType for &Function {
    fn result_type(&self) -> &TyIdx {
        &self.return_type
    }
}

impl ResultType for &mut Function {
    fn result_type(&self) -> &TyIdx {
        &self.return_type
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct FunctionIdx(usize);

impl From<usize> for FunctionIdx {
    fn from(i: usize) -> FunctionIdx {
        FunctionIdx(i)
    }
}

impl From<FunctionIdx> for usize {
    fn from(i: FunctionIdx) -> usize {
        i.0
    }
}

impl From<&FunctionIdx> for usize {
    fn from(i: &FunctionIdx) -> usize {
        i.0
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_function_new() {
        let rt = TyIdx::from(13);
        let mut fun = Function::new(rt.clone(), Location::test());
        assert!(fun.is_empty());

        fun.push_clause(Vec::new(), BlockIdx::from(123), rt);

        assert!(!fun.is_empty());
    }
}
