use codespan::{ByteIndex, Span};
use huia_parser::input_location::InputLocation;
use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub struct Location {
    location: InputLocation,
    source_path: String,
}

impl Location {
    pub fn new(location: InputLocation, source_path: &str) -> Location {
        Location {
            location,
            source_path: source_path.to_string(),
        }
    }

    #[cfg(test)]
    pub fn test() -> Location {
        Location {
            location: InputLocation::pos(0),
            source_path: String::from("Test Example"),
        }
    }

    pub fn into_span(self) -> Option<Span<ByteIndex>> {
        match self.location {
            InputLocation::Span(start, end) => {
                Some(Span::new(ByteIndex(start as u32), ByteIndex(end as u32)))
            }
            _ => None,
        }
    }

    pub fn into_byteindex(self) -> Option<ByteIndex> {
        match self.location {
            InputLocation::Pos(pos) => Some(ByteIndex(pos as u32)),
            _ => None,
        }
    }

    pub fn is_pos(&self) -> bool {
        match self.location {
            InputLocation::Span(..) => true,
            _ => false,
        }
    }

    pub fn is_span(&self) -> bool {
        match self.location {
            InputLocation::Pos(_) => true,
            _ => false,
        }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.source_path, self.location)
    }
}

pub trait Locatable {
    fn location(&self) -> &Location;
}
