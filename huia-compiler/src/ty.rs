use crate::location::Location;
use crate::stable::StringIdx;
use std::collections::BTreeMap;

/// The main "Type" struct.
/// Holds information about Huia Types and Traits.
///
/// FIXME:
/// Should probably support the Native option/result types.
#[derive(Debug, PartialEq)]
pub struct Ty {
    name: Option<StringIdx>,
    location: Option<Location>,
    kind: TyKind,
    inner: TyInner,
}

impl Ty {
    pub fn get_dependencies(&self) -> Option<Vec<&TyIdx>> {
        match self.inner {
            TyInner::Trait { ref dependencies } => Some(dependencies.iter().collect()),
            _ => None,
        }
    }

    pub fn get_location(&self) -> Option<&Location> {
        self.location.as_ref()
    }

    pub fn get_prop_type(&self, name: StringIdx) -> Option<&TyIdx> {
        match self.inner {
            TyInner::Type { ref properties } => properties.get(&name),
            _ => None,
        }
    }

    pub fn get_properties(&self) -> Option<Vec<(&StringIdx, &TyIdx)>> {
        match self.inner {
            TyInner::Type { ref properties } => Some(properties.iter().collect()),
            _ => None,
        }
    }

    pub fn impl_of(&self) -> Option<&TyIdx> {
        match self.inner {
            TyInner::Impl { ref tr, .. } => Some(tr),
            _ => None,
        }
    }

    pub fn impl_for(&self) -> Option<&TyIdx> {
        match self.inner {
            TyInner::Impl { ref ty, .. } => Some(ty),
            _ => None,
        }
    }

    pub fn is_anonymous(&self) -> bool {
        match self.name {
            None => true,
            _ => false,
        }
    }

    pub fn is_impl(&self) -> bool {
        match self.kind {
            TyKind::Impl => true,
            _ => false,
        }
    }

    pub fn is_native_array(&self) -> bool {
        match self.inner {
            TyInner::NativeArray => true,
            _ => false,
        }
    }

    pub fn is_native_atom(&self) -> bool {
        match self.inner {
            TyInner::NativeAtom => true,
            _ => false,
        }
    }

    pub fn is_native_boolean(&self) -> bool {
        match self.inner {
            TyInner::NativeBoolean => true,
            _ => false,
        }
    }

    pub fn is_native_float(&self) -> bool {
        match self.inner {
            TyInner::NativeFloat => true,
            _ => false,
        }
    }

    pub fn is_native_integer(&self) -> bool {
        match self.inner {
            TyInner::NativeInteger => true,
            _ => false,
        }
    }

    pub fn is_native_map(&self) -> bool {
        match self.inner {
            TyInner::NativeMap => true,
            _ => false,
        }
    }

    pub fn is_native_string(&self) -> bool {
        match self.inner {
            TyInner::NativeString => true,
            _ => false,
        }
    }

    pub fn is_native_type(&self) -> bool {
        match self.inner {
            TyInner::NativeType => true,
            _ => false,
        }
    }

    pub fn is_resolved(&self) -> bool {
        match self.kind {
            TyKind::Variable => true,
            _ => false,
        }
    }

    pub fn is_unresolved(&self) -> bool {
        match self.kind {
            TyKind::Unresolved => true,
            _ => false,
        }
    }

    pub fn is_type(&self) -> bool {
        match self.kind {
            TyKind::Native => true,
            TyKind::Type => true,
            _ => false,
        }
    }

    pub fn is_trait(&self) -> bool {
        match self.kind {
            TyKind::Trait => true,
            _ => false,
        }
    }

    pub fn kind(&self) -> &TyKind {
        &self.kind
    }

    pub fn name(&self) -> Option<StringIdx> {
        self.name
    }

    pub fn new_impl(ty: TyIdx, tr: TyIdx, location: Location) -> Ty {
        Ty {
            name: None,
            location: Some(location),
            kind: TyKind::Impl,
            inner: TyInner::Impl { ty, tr },
        }
    }

    pub fn new_reference(name: StringIdx, location: Location) -> Ty {
        Ty {
            name: Some(name),
            location: Some(location),
            kind: TyKind::Unresolved,
            inner: TyInner::Empty,
        }
    }

    pub fn new_type(
        name: StringIdx,
        location: Location,
        properties: Vec<(StringIdx, TyIdx)>,
    ) -> Ty {
        let properties = properties
            .iter()
            .fold(BTreeMap::new(), |mut props, (k, v)| {
                props.insert(k.clone(), v.clone());
                props
            });
        Ty {
            name: Some(name),
            location: Some(location),
            kind: TyKind::Type,
            inner: TyInner::Type { properties },
        }
    }

    pub fn new_trait(name: Option<StringIdx>, location: Location, dependencies: Vec<TyIdx>) -> Ty {
        Ty {
            name,
            location: Some(location),
            kind: TyKind::Trait,
            inner: TyInner::Trait { dependencies },
        }
    }

    pub fn unknown_type(location: Location) -> Ty {
        Ty {
            name: None,
            location: Some(location),
            kind: TyKind::Unresolved,
            inner: TyInner::Empty,
        }
    }

    pub fn native_atom(name: StringIdx) -> Ty {
        Ty {
            name: Some(name),
            location: None,
            kind: TyKind::Native,
            inner: TyInner::NativeAtom,
        }
    }

    pub fn native_array(name: StringIdx) -> Ty {
        Ty {
            name: Some(name),
            location: None,
            kind: TyKind::Native,
            inner: TyInner::NativeArray,
        }
    }

    pub fn native_boolean(name: StringIdx) -> Ty {
        Ty {
            name: Some(name),
            location: None,
            kind: TyKind::Native,
            inner: TyInner::NativeBoolean,
        }
    }

    pub fn native_float(name: StringIdx) -> Ty {
        Ty {
            name: Some(name),
            location: None,
            kind: TyKind::Native,
            inner: TyInner::NativeFloat,
        }
    }

    pub fn native_integer(name: StringIdx) -> Ty {
        Ty {
            name: Some(name),
            location: None,
            kind: TyKind::Native,
            inner: TyInner::NativeInteger,
        }
    }

    pub fn native_map(name: StringIdx) -> Ty {
        Ty {
            name: Some(name),
            location: None,
            kind: TyKind::Native,
            inner: TyInner::NativeMap,
        }
    }

    pub fn native_string(name: StringIdx) -> Ty {
        Ty {
            name: Some(name),
            location: None,
            kind: TyKind::Native,
            inner: TyInner::NativeString,
        }
    }

    pub fn native_type(name: StringIdx) -> Ty {
        Ty {
            name: Some(name),
            location: None,
            kind: TyKind::Native,
            inner: TyInner::NativeType,
        }
    }

    pub fn resolve(&mut self, target: &TyIdx) {
        match self.kind {
            TyKind::Unresolved => {
                self.kind = TyKind::Variable;
                self.inner = TyInner::Variable {
                    target: target.clone(),
                };
            }
            TyKind::Variable => {
                if self.target().unwrap() == target {
                    return;
                }
                panic!("Attempt to resolve already resolved type");
            }
            _ => panic!("Attempt to resolve unresolvable type"),
        }
    }

    pub fn target(&self) -> Option<&TyIdx> {
        match self.inner {
            TyInner::Variable { ref target } => Some(target),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Ord, Eq)]
pub struct TyIdx(usize);

impl From<usize> for TyIdx {
    fn from(i: usize) -> TyIdx {
        TyIdx(i)
    }
}

impl From<TyIdx> for usize {
    fn from(i: TyIdx) -> usize {
        i.0
    }
}

impl From<&TyIdx> for usize {
    fn from(i: &TyIdx) -> usize {
        i.0
    }
}

#[derive(Debug, PartialEq)]
pub enum TyKind {
    Native,
    Type,
    Trait,
    Impl,
    Unresolved,
    Variable,
}

#[derive(Debug, Clone, PartialEq)]
enum TyInner {
    Empty,
    Impl {
        tr: TyIdx,
        ty: TyIdx,
    },
    NativeArray,
    NativeAtom,
    NativeBoolean,
    NativeFloat,
    NativeInteger,
    NativeMap,
    NativeString,
    NativeType,
    Trait {
        dependencies: Vec<TyIdx>,
    },
    Type {
        properties: BTreeMap<StringIdx, TyIdx>,
    },
    Variable {
        target: TyIdx,
    },
}

pub trait ResultType {
    fn result_type(&self) -> &TyIdx;
}
