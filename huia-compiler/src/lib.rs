mod block;
mod clause;
mod context;
mod env;
mod error;
mod function;
mod improvements;
mod ir;
mod location;
mod method;
mod stable;
mod ty;

use crate::context::Context;
use huia_parser::ast::Term;
use std::fs;

pub fn compile_file(path: &str) -> Context {
    let contents = fs::read_to_string(path)
        .unwrap_or_else(|e| panic!("Unable to open file '{}': {}", path, e));
    let terms = Term::file(&contents).expect("Unable to parse file");
    let mut context = Context::new(path);
    let mut builder = ir::builder::Builder::default();

    for term in terms {
        builder.build(term, &mut context);
    }

    context.improve_ir(|mut ir, mut context| {
        improvements::infix_folding::pass(&mut ir, &mut context);
        improvements::desugar_infix::pass(&mut ir, &mut context);
        improvements::unary_folding::pass(&mut ir, &mut context);
        improvements::desugar_unary::pass(&mut ir, &mut context);
    });

    context.improve_functions(|mut function, mut context| {
        improvements::block_result_types::pass(&mut function, &mut context);
        improvements::clause_return_types::pass(&mut function, &mut context);
    });

    context.improve_methods(|mut method, mut context| {
        improvements::block_result_types::pass(&mut method, &mut context);
        improvements::clause_return_types::pass(&mut method, &mut context);
    });

    improvements::no_unresolved_types_remaining::pass(&mut context);

    context
}
