use string_interner::{StringInterner, Sym};

#[derive(Debug, Default)]
pub struct StringTable(StringInterner<Sym>);

impl StringTable {
    pub fn resolve(&self, idx: StringIdx) -> Option<&str> {
        self.0.resolve(idx.into())
    }

    #[allow(dead_code)]
    pub fn get<T: Into<String> + AsRef<str>>(&self, value: T) -> Option<StringIdx> {
        match self.0.get(value) {
            Some(syn) => Some(syn.into()),
            None => None,
        }
    }

    pub fn intern<T: Into<String> + AsRef<str>>(&mut self, value: T) -> StringIdx {
        self.0.get_or_intern(value).into()
    }

    #[cfg(test)]
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord, Copy)]
pub struct StringIdx(Sym);

impl From<Sym> for StringIdx {
    fn from(s: Sym) -> StringIdx {
        StringIdx(s)
    }
}

impl From<StringIdx> for Sym {
    fn from(s: StringIdx) -> Sym {
        s.0
    }
}

impl From<&StringIdx> for Sym {
    fn from(s: &StringIdx) -> Sym {
        s.0
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_default() {
        let stable = StringTable::default();
        assert!(stable.is_empty());
    }

    #[test]
    fn test_insert() {
        let mut stable = StringTable::default();
        assert!(stable.is_empty());

        let idx0 = stable.intern("Marty McFly");
        let idx1 = stable.intern("Marty McFly");
        assert_eq!(idx0, idx1);
    }

    #[test]
    fn test_resolve() {
        let mut stable = StringTable::default();
        let idx = stable.intern("Marty McFly");
        assert_eq!(stable.resolve(idx).unwrap(), "Marty McFly");
    }
}
