use crate::location::Location;
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct CompileError {
    message: String,
    location: Location,
    kind: ErrorKind,
}

impl CompileError {
    pub fn new(message: &str, location: Location, kind: ErrorKind) -> CompileError {
        let message = message.to_string();
        CompileError {
            message,
            location,
            kind,
        }
    }

    pub fn message(&self) -> &str {
        &self.message
    }

    pub fn location(&self) -> &Location {
        &self.location
    }

    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }
}

impl fmt::Display for CompileError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Compile Error: {:?}", self)
    }
}

impl Error for CompileError {
    fn description(&self) -> &str {
        &self.message
    }
}

#[derive(Debug)]
pub enum ErrorKind {
    TypeRedefined,
    UnknownVariable,
    InconsistentBranchTypes,
    TypeInferenceFailure,
    UnknownType,
}
