use crate::block::{Block, BlockIdx};
use crate::error::{CompileError, ErrorKind};
use crate::function::{Function, FunctionIdx};
use crate::ir::IR;
use crate::location::Location;
use crate::method::{Method, MethodIdx, Modifier};
use crate::stable::{StringIdx, StringTable};
use crate::ty::{Ty, TyIdx};
use huia_parser::input_location::InputLocation;
mod display;

/// The compiler context.
///
/// This is the god-object of the compiler, holding all of the state of the
/// compiler during the various passes and code generation.
#[derive(Debug)]
pub struct Context {
    blocks: Vec<Block>,
    current_file: StringIdx,
    errors: Vec<CompileError>,
    functions: Vec<Function>,
    methods: Vec<Method>,
    strings: StringTable,
    types: Vec<Ty>,
}

impl Context {
    /// Declare an anonymous trait as an aggregate of other types.
    pub fn anonymous_trait(&mut self, requirements: Vec<TyIdx>, location: Location) -> TyIdx {
        // If there's only one type in the requirement we can just return a reference to that instead.
        if requirements.len() == 1 {
            return requirements[0].clone();
        }

        // If there's already an anonymous trait with the same dependencies then return a reference to that instead.
        if let Some(idx) = self.find_type_index(|ty| {
            ty.is_trait()
                && ty.is_anonymous()
                && ty.get_dependencies() == Some(requirements.iter().collect())
        }) {
            return idx;
        }

        let idx = self.types.len();
        let ty = Ty::new_trait(None, location, requirements);
        self.types.push(ty);
        idx.into()
    }

    pub fn compile_error(&mut self, message: &str, location: Location, kind: ErrorKind) {
        self.errors.push(CompileError::new(message, location, kind));
    }

    /// Interns a string into the context and returns a reference to it.
    pub fn constant_string(&mut self, value: &str) -> StringIdx {
        self.strings.intern(value)
    }

    pub fn current_file(&self) -> &str {
        self.strings.resolve(self.current_file).unwrap()
    }

    /// Define a function in the context and return a reference to it.
    pub fn define_function(&mut self, function: Function) -> FunctionIdx {
        let idx = self.functions.len();
        self.functions.push(function);
        idx.into()
    }

    /// Define a method in the context and return a reference to it.
    pub fn define_method(&mut self, method: Method) -> MethodIdx {
        let idx = self.methods.len();
        self.methods.push(method);
        idx.into()
    }

    /// Declare an trait implementation for a specific type.
    pub fn declare_impl(&mut self, tr: TyIdx, ty: TyIdx, location: Location) -> TyIdx {
        let idx = self.types.len();
        let ty = Ty::new_impl(ty, tr, location);
        self.types.push(ty);
        idx.into()
    }

    /// Declare a trait type.
    pub fn declare_trait(
        &mut self,
        name: StringIdx,
        requirements: Vec<TyIdx>,
        location: Location,
    ) -> TyIdx {
        if self.find_type_index(|ty| ty.name() == Some(name)).is_some() {
            let message = format!("Type {} redefined", self.get_string(name).unwrap());
            self.compile_error(&message, location.clone(), ErrorKind::TypeRedefined);
        }

        let idx = self.types.len();
        let ty = Ty::new_trait(Some(name), location, requirements);
        self.types.push(ty);
        idx.into()
    }

    /// Declare a user type.
    pub fn declare_type(
        &mut self,
        name: StringIdx,
        properties: Vec<(StringIdx, TyIdx)>,
        location: Location,
    ) -> TyIdx {
        if self
            .find_type_index(|ty| ty.name() == Some(name) && !ty.is_unresolved())
            .is_some()
        {
            let message = format!("Type {} redefined", self.get_string(name).unwrap());
            self.compile_error(&message, location.clone(), ErrorKind::TypeRedefined)
        }

        let idx = self.types.len();
        let ty = Ty::new_type(name, location, properties);
        self.types.push(ty);
        idx.into()
    }

    /// Retrieve a method by it's holding type and name, if possible.
    pub fn find_method(
        &self,
        parent_type: &TyIdx,
        name: StringIdx,
        modifier: Modifier,
    ) -> Option<MethodIdx> {
        for (idx, method) in self.methods.iter().enumerate() {
            if method.name() == &name
                && method.parent_type() == parent_type
                && method.modifier() == modifier
            {
                return Some(MethodIdx::from(idx));
            }
        }
        None
    }

    #[cfg(test)]
    pub fn find_type_by_name(&self, name: &str) -> Option<&Ty> {
        match self.strings.get(name) {
            Some(idx) => self.find_type(|ty| ty.name() == Some(idx)),
            None => None,
        }
    }

    /// Return the first type which matches the predicate.
    pub fn find_type<F: Fn(&Ty) -> bool>(&self, predicate: F) -> Option<&Ty> {
        match self.find_type_index(predicate) {
            Some(idx) => self.types.get(usize::from(idx)),
            None => None,
        }
    }

    /// Return the index of the first type that matches the predicate
    pub fn find_type_index<F: Fn(&Ty) -> bool>(&self, predicate: F) -> Option<TyIdx> {
        for (idx, ty) in self.types.iter().enumerate() {
            if predicate(ty) {
                return Some(idx.into());
            }
        }

        None
    }

    /// Retrieve a specific block by it's index.
    pub fn get_block(&self, idx: &BlockIdx) -> Option<&Block> {
        self.blocks.get(usize::from(idx))
    }

    pub fn get_block_mut(&mut self, idx: &BlockIdx) -> Option<&mut Block> {
        self.blocks.get_mut(usize::from(idx))
    }

    /// Retrieve a specific function by it's index.
    pub fn get_function(&self, idx: FunctionIdx) -> Option<&Function> {
        self.functions.get(usize::from(idx))
    }

    pub fn get_method(&self, idx: MethodIdx) -> Option<&Method> {
        self.methods.get(usize::from(idx))
    }

    pub fn get_method_mut(&mut self, idx: MethodIdx) -> Option<&mut Method> {
        self.methods.get_mut(usize::from(idx))
    }

    pub fn get_string(&self, idx: StringIdx) -> Option<&str> {
        self.strings.resolve(idx)
    }

    /// Retrieve a specific type by it's index.
    pub fn get_type(&self, idx: &TyIdx) -> Option<&Ty> {
        self.types.get(usize::from(idx))
    }

    /// Retrieve a specific type by it's index for mutation.
    pub fn get_type_mut(&mut self, idx: &TyIdx) -> Option<&mut Ty> {
        self.types.get_mut(usize::from(idx))
    }

    /// Perform blockwise improvement.
    ///
    /// Iterates through all blocks in the context applying a provided improver
    /// function.
    pub fn improve_blocks<F: Fn(&mut Block, &mut Context)>(&mut self, improver: F) {
        let mut blocks = self.blocks.clone();

        for block in blocks.iter_mut() {
            improver(block, self);
        }

        self.blocks = blocks;
    }

    /// Perform a function-wise improvement.
    pub fn improve_functions<F: Fn(&mut Function, &mut Context)>(&mut self, improver: F) {
        let mut functions = self.functions.clone();

        for function in functions.iter_mut() {
            improver(function, self);
        }

        self.functions = functions;
    }

    /// Perform IR-wise improvement.
    ///
    /// Iterates through all blocks in the context and all IRs in each block
    /// applying the provided improver fuction.
    ///
    /// See `IR.improve` for more information.
    pub fn improve_ir<F: Fn(&mut IR, &mut Context)>(&mut self, improver: F) {
        // FIXME: is there a way to not have to copy *all* the source code in
        // memory?
        let mut blocks = self.blocks.clone();

        for block in blocks.iter_mut() {
            for ir in block.iter_mut() {
                ir.improve(self, &improver);
            }
        }

        self.blocks = blocks;
    }

    /// Perform a method-wise improvement.
    pub fn improve_methods<F: Fn(&mut Method, &mut Context)>(&mut self, improver: F) {
        let mut methods = self.methods.clone();

        for method in methods.iter_mut() {
            improver(method, self);
        }

        self.methods = methods;
    }

    /// Convert an AST `InputLocation` into a compiler `Location`.
    pub fn location(&self, location: &InputLocation) -> Location {
        let path = self.strings.resolve(self.current_file).unwrap();
        Location::new(location.clone(), path)
    }

    /// Create a new compiler context for the specified source file.
    pub fn new(path: &str) -> Context {
        let mut strings = StringTable::default();
        let blocks = Vec::default();
        let current_file = strings.intern(path);
        let errors = Vec::default();
        let functions = Vec::default();
        // let types = Vec::default();
        let types = vec![
            Ty::native_array(strings.intern("Huia.Native.Array")),
            Ty::native_atom(strings.intern("Huia.Native.Atom")),
            Ty::native_boolean(strings.intern("Huia.Native.Boolean")),
            Ty::native_float(strings.intern("Huia.Native.Float")),
            Ty::native_integer(strings.intern("Huia.Native.Integer")),
            Ty::native_map(strings.intern("Huia.Native.Map")),
            Ty::native_string(strings.intern("Huia.Native.String")),
            Ty::native_type(strings.intern("Huia.Native.Type")),
        ];
        let methods = Vec::default();
        Context {
            blocks,
            current_file,
            errors,
            functions,
            methods,
            strings,
            types,
        }
    }

    pub fn next_block_idx(&self) -> BlockIdx {
        self.blocks.len().into()
    }

    /// Append a block to the context and return it's index.
    pub fn push_block(&mut self, block: Block) -> BlockIdx {
        let idx = self.blocks.len();
        self.blocks.push(block);
        idx.into()
    }

    /// Create a (potentially unresolved) type reference based on the name of
    /// the specified type.
    pub fn reference_type(&mut self, name: StringIdx, location: Location) -> TyIdx {
        // Try and eagerly dereference types if we can.
        // This can theoretically return another type reference, so we still
        // have to do a full type-check.
        for (i, ty) in self.types.iter().enumerate() {
            if ty.name() == Some(name) {
                return i.into();
            }
        }

        // Otherwise create a new type reference.
        let idx = self.types.len();
        let ty = Ty::new_reference(name, location);
        self.types.push(ty);
        idx.into()
    }

    #[cfg(test)]
    pub fn test() -> Context {
        Context::new("Test context")
    }

    pub fn types(&self) -> Vec<&Ty> {
        self.types.iter().collect()
    }

    /// Create a type variable of unknown type.
    ///
    /// Unresolved types should be removed by typechecking and any remaining
    /// unresolved types will become compiler errors.
    pub fn unknown_type(&mut self, location: Location) -> TyIdx {
        let idx = self.types.len();
        let ty = Ty::unknown_type(location);
        self.types.push(ty);
        idx.into()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_types() {
        let mut context = Context::test();
        let s0 = context.constant_string("Marty McFly");
        let s1 = context.constant_string("Doc Brown");

        let t0 = context.declare_type(s0, Vec::new(), Location::test());
        let t1 = context.reference_type(s0, Location::test());
        let t2 = context.reference_type(s1, Location::test());

        assert_eq!(t0, t1);
        assert_ne!(t0, t2);

        let ty0 = context.get_type(&t0).unwrap();
        let ty1 = context.get_type(&t1).unwrap();
        let ty2 = context.get_type(&t2).unwrap();

        assert_eq!(ty0, ty1);
        assert_ne!(ty0, ty2);

        assert!(ty0.is_type());
        assert!(!ty0.is_trait());
        assert!(!ty0.is_unresolved());
        assert!(ty1.is_type());
        assert!(!ty1.is_trait());
        assert!(!ty1.is_unresolved());
        assert!(!ty2.is_type());
        assert!(!ty2.is_trait());
        assert!(ty2.is_unresolved());
    }

    #[test]
    fn test_type_definition() {
        let mut context = Context::test();

        let s = context.constant_string("Marty McFly");

        context.declare_type(s, Vec::default(), Location::test());
        context.reference_type(s, Location::test());

        assert!(context.errors.is_empty());

        context.declare_trait(s, Vec::default(), Location::test());

        assert!(!context.errors.is_empty());
    }
}
