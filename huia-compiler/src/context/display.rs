use super::Context;
use crate::block::BlockIdx;
use crate::clause::Clauseable;
use crate::ir::IR;
use crate::location::Locatable;
use crate::ty::ResultType;
use crate::ty::TyIdx;
use std::fmt;

const VERSION: &str = env!("CARGO_PKG_VERSION");

impl fmt::Display for Context {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut buffer = String::new();

        fmt_header(&mut buffer, &self);
        fmt_errors(&mut buffer, &self);
        fmt_types(&mut buffer, &self);
        fmt_functions(&mut buffer, &self);
        fmt_methods(&mut buffer, &self);

        write!(f, "{}", buffer)
    }
}

fn fmt_header(buffer: &mut String, context: &Context) {
    buffer.push_str(&format!(
        "# Huia Compiler v{} IR for {:?}\n",
        VERSION,
        context.current_file()
    ));
}

fn fmt_errors(buffer: &mut String, context: &Context) {
    let len = context.errors.len();

    if len > 0 {
        buffer.push_str(&format!("#\n# Found {} errors:\n#\n", len));

        for error in &context.errors {
            buffer.push_str(&format!(
                "# {:?}: {} @ {}\n",
                error.kind(),
                error.message(),
                error.location()
            ));
        }
    }
}

fn fmt_ty_name(idx: &TyIdx, context: &Context) -> String {
    if let Some(ty) = context.get_type(idx) {
        if let Some(name) = ty.name() {
            return context.get_string(name).unwrap().to_string();
        }
        if ty.is_impl() {
            let tr = fmt_ty_name(ty.impl_of().unwrap(), &context);
            let ty = fmt_ty_name(ty.impl_for().unwrap(), &context);
            return format!("{}<{}>", tr, ty);
        }
        if ty.is_resolved() {
            let ty = fmt_ty_name(ty.target().unwrap(), &context);
            return format!("&{}", ty);
        }
    }
    format!("T{:0>4}", usize::from(idx))
}

#[allow(clippy::cyclomatic_complexity)]
fn fmt_types(buffer: &mut String, context: &Context) {
    if !context.types.is_empty() {
        buffer.push_str("\n# Types:\n");
    }

    for (i, ty) in context.types.iter().enumerate() {
        if let Some(name) = ty.name() {
            let name = context.get_string(name).unwrap();
            buffer.push_str(&format!("{} = ", name));
        } else if ty.is_impl() {
            let tr = fmt_ty_name(ty.impl_of().unwrap(), &context);
            let ty = fmt_ty_name(ty.impl_for().unwrap(), &context);
            buffer.push_str(&format!("{}<{}> = ", tr, ty));
        } else if ty.is_resolved() {
            let ty = fmt_ty_name(ty.target().unwrap(), &context);
            buffer.push_str(&format!("&{} = ", ty));
        } else {
            buffer.push_str(&format!("T{:0>4} = ", i));
        };

        if ty.is_native_atom() {
            buffer.push_str("native<atom>");
        } else if ty.is_native_array() {
            buffer.push_str("native<array>");
        } else if ty.is_native_boolean() {
            buffer.push_str("native<bool>");
        } else if ty.is_native_float() {
            buffer.push_str("native<f64>");
        } else if ty.is_native_integer() {
            buffer.push_str("native<i64>");
        } else if ty.is_native_map() {
            buffer.push_str("native<map>");
        } else if ty.is_native_string() {
            buffer.push_str("native<string>");
        } else if ty.is_native_type() {
            buffer.push_str("native<type>");
        } else if ty.is_type() {
            buffer.push_str("type");

            if let Some(properties) = ty.get_properties() {
                if !properties.is_empty() {
                    for (i, (name, ty)) in properties.iter().enumerate() {
                        if i == 0 {
                            buffer.push_str("<");
                        }

                        let name = context.get_string(**name).unwrap();
                        let ty = fmt_ty_name(ty, &context);

                        buffer.push_str(&format!("{}: {}", name, ty));

                        if i == properties.len() - 1 {
                            buffer.push_str(">");
                        } else {
                            buffer.push_str(", ");
                        }
                    }
                }
            }
        } else if ty.is_trait() {
            if ty.is_anonymous() {
                buffer.push_str("anonymous ");
            }
            buffer.push_str("trait");

            if let Some(dependencies) = ty.get_dependencies() {
                if !dependencies.is_empty() {
                    for (i, dep) in dependencies.iter().enumerate() {
                        if i == 0 {
                            buffer.push_str("<");
                        }

                        buffer.push_str(&fmt_ty_name(dep, &context));

                        if i == dependencies.len() - 1 {
                            buffer.push_str(">");
                        } else {
                            buffer.push_str(", ");
                        }
                    }
                }
            }
        } else if ty.is_impl() {
            let tr = fmt_ty_name(ty.impl_of().unwrap(), &context);
            let ty = fmt_ty_name(ty.impl_for().unwrap(), &context);
            buffer.push_str(&format!("impl {} for {}", tr, ty));
        } else if ty.is_resolved() {
            let target_ty = fmt_ty_name(ty.target().unwrap(), &context);
            buffer.push_str(&format!("alias for {}", target_ty));
        } else if ty.is_unresolved() {
            buffer.push_str("unknown type");
        }
        if let Some(location) = ty.get_location() {
            buffer.push_str(&format!(" defined at {}\n", location));
        } else {
            buffer.push_str("\n");
        }
    }
}

fn fmt_functions(mut buffer: &mut String, context: &Context) {
    if !context.functions.is_empty() {
        buffer.push_str("\n# Anonymous Functions:\n");
    }

    for (i, fun) in context.functions.iter().enumerate() {
        buffer.push_str(&format!("# defined @ {}\n", fun.location()));
        for clause in fun.clauses() {
            buffer.push_str(&format!("f{}", i));
            let args = clause.arguments();
            if args.is_empty() {
                buffer.push_str("()");
            } else {
                for (i, (name, ty)) in args.iter().enumerate() {
                    if i == 0 {
                        buffer.push_str("(");
                    }

                    let name = context.get_string(**name).unwrap();
                    let ty = fmt_ty_name(ty, &context);

                    buffer.push_str(&format!("{}: {}", name, ty));

                    if i == args.len() - 1 {
                        buffer.push_str(")");
                    } else {
                        buffer.push_str(", ");
                    }
                }
            }
            buffer.push_str(&format!(
                " -> {}\n",
                fmt_ty_name(clause.result_type(), &context)
            ));

            let mut jumps = Vec::new();

            fmt_block(&mut buffer, &context, clause.body(), &mut jumps);

            buffer.push_str("\n");
        }
    }
}

fn fmt_block_name(idx: &BlockIdx) -> String {
    format!("block_{:0>4}", usize::from(idx))
}

fn fmt_block(
    mut buffer: &mut String,
    context: &Context,
    idx: &BlockIdx,
    mut jumps: &mut Vec<BlockIdx>,
) {
    if let Some(block) = context.get_block(idx) {
        buffer.push_str(&format!("  {}:", fmt_block_name(idx)));

        buffer.push_str(&format!(
            " -> {}",
            fmt_ty_name(block.result_type(), &context)
        ));

        buffer.push_str("\n");

        let mut i: usize = 0;
        for ir in block.ir_ref() {
            fmt_ir(&mut buffer, &context, &ir, &mut i, &mut jumps);
        }
    } else {
        buffer.push_str("  ! unknown block");
    }
}

#[allow(clippy::cyclomatic_complexity)]
fn fmt_ir(
    mut buffer: &mut String,
    context: &Context,
    ir: &IR,
    mut i: &mut usize,
    mut jumps: &mut Vec<BlockIdx>,
) {
    if ir.is_call() {
        let (ir, args) = ir.get_call().unwrap();
        fmt_ir(&mut buffer, &context, ir, &mut i, &mut jumps);
        let callee = *i;
        let mut ivs = Vec::new();
        for arg in args {
            fmt_ir(&mut buffer, &context, arg, &mut i, &mut jumps);
            ivs.push(*i);
        }
        buffer.push_str(&format!("    v{:0>2}: v{:0>2}", *i, callee));
        if ivs.is_empty() {
            buffer.push_str("()");
        }
        for (j, k) in ivs.iter().enumerate() {
            if j == 0 {
                buffer.push_str("(");
            }

            buffer.push_str(&format!("v{:0>2}", k));

            if j == ivs.len() - 1 {
                buffer.push_str(")");
            } else {
                buffer.push_str(", ");
            }
        }
    } else if ir.is_constant() {
        let value = ir.get_value().unwrap();
        if value.is_array() {
            let mut ivs = Vec::new();
            for ir in value.array().unwrap() {
                fmt_ir(&mut buffer, &context, ir, &mut i, &mut jumps);
                ivs.push(*i);
            }
            *i += 1;
            buffer.push_str(&format!("    v{:0>2}: ", *i));
            for (j, k) in ivs.iter().enumerate() {
                if j == 0 {
                    buffer.push_str("[");
                }

                buffer.push_str(&format!("v{:0>2}", k));

                if j == ivs.len() - 1 {
                    buffer.push_str("]");
                } else {
                    buffer.push_str(", ");
                }
            }
        } else if value.is_atom() {
            let atom = value.atom().unwrap();
            let atom = context.get_string(*atom).unwrap();
            *i += 1;
            buffer.push_str(&format!("    v{:0>2}: {}", i, atom));
        } else if value.is_boolean() {
            let boolean = value.boolean().unwrap();
            *i += 1;
            buffer.push_str(&format!("    v{:0>2}: {}", i, boolean));
        } else if value.is_float() {
            let float = value.float().unwrap();
            *i += 1;
            buffer.push_str(&format!("    v{:0>2}: {}", i, float));
        } else if value.is_integer() {
            let int = value.integer().unwrap();
            *i += 1;
            buffer.push_str(&format!("    v{:0>2}: {}", i, int));
        } else if value.is_map() {
            let map = value.map().unwrap();
            let mut ivs = Vec::new();
            for (key, val) in map {
                fmt_ir(&mut buffer, &context, key, &mut i, &mut jumps);
                ivs.push(*i);
                fmt_ir(&mut buffer, &context, val, &mut i, &mut jumps);
                ivs.push(*i);
            }
            *i += 1;
            buffer.push_str(&format!("    v{:0>2}: ", *i));
            for (j, k) in ivs.iter().enumerate() {
                if j == 0 {
                    buffer.push_str("{");
                }

                buffer.push_str(&format!("v{:0>2}", k));

                if j == ivs.len() - 1 {
                    buffer.push_str("}");
                } else if j % 2 == 0 {
                    buffer.push_str(" : ");
                } else {
                    buffer.push_str(", ");
                }
            }
        } else if value.is_string() {
            let string = value.string().unwrap();
            let string = context.get_string(*string).unwrap();
            *i += 1;
            buffer.push_str(&format!("    v{:0>2}: {:?}", *i, string));
        }
    } else if ir.is_constructor() {
        let constructor = ir.get_constructor().unwrap();
        let mut ivs = Vec::new();
        for (_key, val) in constructor {
            fmt_ir(&mut buffer, &context, val, &mut i, &mut jumps);
            ivs.push(*i);
        }
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: ", *i));
        buffer.push_str(&fmt_ty_name(&ir.result_type(), &context));
        for (j, (key, _val)) in constructor.iter().enumerate() {
            if j == 0 {
                buffer.push_str("{");
            }

            let key = context.get_string(*key).unwrap();
            buffer.push_str(&format!("{}: v{:0>2}", key, ivs[j]));

            if j == ivs.len() - 1 {
                buffer.push_str("}");
            } else {
                buffer.push_str(", ");
            }
        }
    } else if ir.is_function() {
        let idx = ir.get_function().unwrap();
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: ref f{}", *i, usize::from(idx)));
    } else if ir.is_get_local() {
        let name = context.get_string(*ir.get_local().unwrap()).unwrap();
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: get {}", *i, name));
    } else if ir.is_get_property() {
        let name = context.get_string(*ir.get_property().unwrap()).unwrap();
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: get @{}", *i, name));
    } else if ir.is_infix() {
        let (op, lhs, rhs) = ir.get_infix().unwrap();
        fmt_ir(&mut buffer, &context, lhs, &mut i, &mut jumps);
        let lhs = *i;
        fmt_ir(&mut buffer, &context, rhs, &mut i, &mut jumps);
        let rhs = *i;
        *i += 1;
        buffer.push_str(&format!(
            "    v{:0>2}: {} v{:0>2} v{:0>2}",
            *i, op, lhs, rhs
        ));
    } else if ir.is_jump() {
        let idx = ir.get_jump().unwrap();
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: jump {}", *i, fmt_block_name(idx)));
    } else if ir.is_jump_if_false() {
        let (ir, idx) = ir.get_jump_if_false().unwrap();
        fmt_ir(&mut buffer, &context, ir, &mut i, &mut jumps);
        *i += 1;
        buffer.push_str(&format!(
            "    v{:0>2}: jump if false {}",
            *i,
            fmt_block_name(idx)
        ));
    } else if ir.is_jump_if_true() {
        let (ir, idx) = ir.get_jump_if_true().unwrap();
        fmt_ir(&mut buffer, &context, ir, &mut i, &mut jumps);
        *i += 1;
        buffer.push_str(&format!(
            "    v{:0>2}: jump if true {}",
            *i,
            fmt_block_name(idx)
        ));
    } else if ir.is_method_call() {
        let (rx, name, args) = ir.get_method_call().unwrap();
        fmt_ir(&mut buffer, &context, rx, &mut i, &mut jumps);
        let rx = *i;
        let name = context.get_string(*name).unwrap();
        let mut ivs = Vec::new();
        for arg in args {
            fmt_ir(&mut buffer, &context, arg, &mut i, &mut jumps);
            ivs.push(*i);
        }
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: v{:0>2}.{}", *i, rx, name));
        if ivs.is_empty() {
            buffer.push_str("()");
        }
        for (j, k) in ivs.iter().enumerate() {
            if j == 0 {
                buffer.push_str("(");
            }

            buffer.push_str(&format!("v{:0>2}", k));

            if j == ivs.len() - 1 {
                buffer.push_str(")");
            } else {
                buffer.push_str(", ");
            }
        }
    } else if ir.is_return() {
        let value = ir.get_return().unwrap();
        fmt_ir(&mut buffer, &context, value, &mut i, &mut jumps);
        let value = *i;
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: return v{:0>2}", *i, value));
    } else if ir.is_set_local() {
        let (name, value) = ir.get_set_local().unwrap();
        fmt_ir(&mut buffer, &context, value, &mut i, &mut jumps);
        let value = *i;
        let name = context.get_string(*name).unwrap();
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: set {} v{:0>2}", *i, name, value));
    } else if ir.is_set_properties() {
        let properties = ir.get_set_properties().unwrap();
        let mut ivs = Vec::new();
        for (_key, value) in properties {
            fmt_ir(&mut buffer, &context, value, &mut i, &mut jumps);
            ivs.push(*i);
        }
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: ", *i));
        for (j, (key, _val)) in properties.iter().enumerate() {
            if j == 0 {
                buffer.push_str("@{");
            }

            let key = context.get_string(*key).unwrap();
            buffer.push_str(&format!("{}: v{:0>2}", key, ivs[j]));

            if j == ivs.len() - 1 {
                buffer.push_str("}");
            } else {
                buffer.push_str(", ");
            }
        }
    } else if ir.is_type_reference() {
        let ty = ir.get_type_reference().unwrap();
        buffer.push_str(&format!("    v{:0>2}: {}", i, fmt_ty_name(ty, &context)));
    } else if ir.is_unary() {
        let (op, rhs) = ir.get_unary().unwrap();
        fmt_ir(&mut buffer, &context, rhs, &mut i, &mut jumps);
        let rhs = *i;
        *i += 1;
        buffer.push_str(&format!("    v{:0>2}: {} v{:0>2}", *i, op, rhs));
    }

    buffer.push_str(&format!(" <{}>", fmt_ty_name(&ir.result_type(), &context)));
    buffer.push_str("\n");

    if let Some(target) = ir.get_target() {
        jumps.push(target.clone());
    }
}

fn fmt_methods(mut buffer: &mut String, context: &Context) {
    if !context.methods.is_empty() {
        buffer.push_str("\n# Methods:\n");
    }

    for method in &context.methods {
        let parent_type = fmt_ty_name(method.parent_type(), &context);
        let name = context.get_string(*method.name()).unwrap();
        let sigil = if method.is_static() { "." } else { "#" };

        for clause in method.clauses() {
            buffer.push_str(&format!("{}{}{}", parent_type, sigil, name));

            let args = clause.arguments();
            if args.is_empty() {
                buffer.push_str("()");
            } else {
                for (j, (name, ty)) in args.iter().enumerate() {
                    if j == 0 {
                        buffer.push_str("(");
                    }

                    let name = context.get_string(**name).unwrap();
                    let ty = fmt_ty_name(ty, &context);

                    buffer.push_str(&format!("{}: {}", name, ty));

                    if j == args.len() - 1 {
                        buffer.push_str(")");
                    } else {
                        buffer.push_str(", ");
                    }
                }
            }
            buffer.push_str(&format!(
                " -> {}\n",
                fmt_ty_name(clause.result_type(), &context)
            ));

            let mut jumps = vec![clause.body().clone()];
            let mut seen = Vec::new();

            while let Some(idx) = jumps.pop() {
                if !seen.iter().any(|idx0| idx0 == &idx) {
                    fmt_block(&mut buffer, &context, &idx, &mut jumps);
                    seen.push(idx);
                    seen.dedup();
                }
            }

            buffer.push_str("\n");
        }
    }
}
