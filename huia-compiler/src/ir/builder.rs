use crate::block::{Block, BlockIdx};
use crate::clause::Clauseable;
use crate::context::Context;
use crate::env::Env;
use crate::error::ErrorKind;
use crate::function::Function;
use crate::ir::{Val, IR};
use crate::location::Locatable;
use crate::location::Location as Loc;
use crate::method::{Method, MethodIdx, Modifier};
use crate::stable::StringIdx;
use crate::ty::{ResultType, TyIdx};
use huia_parser::ast::{Identifier, Location, NodeType, Term, TypeSpec, Value};

/// The Builder is a simple stack machine for converting AST into IR.
#[derive(Debug)]
pub struct Builder {
    blocks: Vec<Block>,
    types: Vec<TyIdx>,
}

impl Builder {
    /// Consume an AST node and build IR.
    ///
    /// Any IR will be on the Builder's stack.
    #[allow(clippy::cyclomatic_complexity)]
    pub fn build(&mut self, node: Term, mut context: &mut Context) {
        match node.node_type() {
            NodeType::Array => {
                let ty_idx = context.constant_string("Huia.Native.Array");
                let location = context.location(&node.location());
                let ty = context.reference_type(ty_idx, location.clone());

                let elements = node
                    .array()
                    .unwrap()
                    .iter()
                    .map(|node| {
                        self.build(node.clone(), &mut context);
                        self.pop_ir().unwrap()
                    })
                    .collect();
                self.push_ir(IR::new_constant(ty, location, Val::Array(elements)));
            }
            NodeType::Atom => {
                let ty_idx = context.constant_string("Huia.Native.Atom");
                let location = context.location(&node.location());
                let ty = context.reference_type(ty_idx, location.clone());
                let idx = context.constant_string(node.atom().unwrap().value_ref().as_str());
                self.push_ir(IR::new_constant(ty, location, Val::Atom(idx)));
            }
            NodeType::Boolean => {
                let ty_idx = context.constant_string("Huia.Native.Boolean");
                let location = context.location(&node.location());
                let ty = context.reference_type(ty_idx, location.clone());
                let value = *node.boolean().unwrap().value_ref();
                self.push_ir(IR::new_constant(ty, location, Val::Boolean(value)));
            }
            NodeType::Binary => {
                let location = context.location(&node.location());
                let (op, lhs, rhs) = node.binary().unwrap();
                self.build(lhs.clone(), &mut context);
                self.build(rhs.clone(), &mut context);
                let rhs = self.pop_ir().unwrap();
                let lhs = self.pop_ir().unwrap();

                let result_type = if op.is_logical() {
                    let name = context.constant_string("Huia.Native.Boolean");
                    context.reference_type(name, location.clone())
                }
                else if op.is_arithmetic() {
                    lhs.result_type().clone()
                }
                else {
                    panic!("Operator {:?} is neither arithmetic nor logical", op);
                };

                self.push_ir(IR::new_infix(
                    result_type,
                    location,
                    op.value_ref().clone(),
                    lhs,
                    rhs,
                ));
            }
            NodeType::Call => {
                let (callee, args) = node.call().unwrap();
                let location = context.location(&node.location());

                self.build(callee.clone(), &mut context);
                let callee = self.pop_ir().unwrap();

                let arguments = args.iter().map(|node| {
                    self.build(node.clone(), &mut context);
                    self.pop_ir().unwrap()
                }).collect();

                let result_type = context.unknown_type(location.clone());

                self.push_ir(IR::new_call(result_type, location, callee, arguments));
            }
            NodeType::Constructor => {
                let (ty, props) = node.constructor().unwrap();
                let ty_idx = context.constant_string(ty.value_ref().as_str());
                let location = context.location(&node.location());
                let ty = context.reference_type(ty_idx, location.clone());

                let properties = props
                    .iter()
                    .map(|(key, value)| {
                        let key = context.constant_string(key.value_ref().as_str());
                        self.build(value.clone(), &mut context);
                        let value = self.pop_ir().unwrap();
                        (key, value)
                    })
                    .collect();
                self.push_ir(IR::new_constructor(ty, location, properties));
            }
            NodeType::Declaration => {
                let location = context.location(&node.location());
                let (name, rhs) = node.declaration().unwrap();
                let name = context.constant_string(name.value_ref().as_str());
                self.build(rhs.clone(), &mut context);
                let rhs = self.pop_ir().unwrap();
                self.env_set(name, &rhs.result_type());
                self.push_ir(IR::new_set_local(rhs.result_type().clone(), location, name, rhs));
            }
            NodeType::Float => {
                let ty_idx = context.constant_string("Huia.Native.Float");
                let location = context.location(&node.location());
                let ty = context.reference_type(ty_idx, location.clone());
                let value = *node.float().unwrap().value_ref();
                self.push_ir(IR::new_constant(ty, location, Val::Float(value)));
            }
            NodeType::Function => {
                let function = node.function().unwrap();
                let location = context.location(&node.location());
                let return_type = context.unknown_type(location.clone());
                let mut fun = Function::new(return_type.clone(), location.clone());


                for clause in function.value_ref() {
                    self.build_function_clause(&mut context, &mut fun, clause.arguments(), clause.body(), location.clone());
                }

                let fun_idx = context.define_function(fun);
                self.push_ir(IR::new_function_ref(return_type, location, fun_idx));
            }
            NodeType::If => {
                let (test, positive, negative) = node.if_expr().unwrap();
                let location = context.location(&node.location());

                self.build(test.clone(), &mut context);
                let test = self.pop_ir().unwrap();
                let result_type = context.unknown_type(location.clone());
                let value_var_name = context.constant_string("$if_tmp");
                self.env_set(value_var_name, &result_type);

                let mut following_block_idx = usize::from(context.next_block_idx());

                if !negative.is_empty() {
                    following_block_idx += 2;
                } else {
                    following_block_idx += 1;
                }

                self.push_block(context.unknown_type(location.clone()));
                for node in positive {
                    self.build(node.clone(), &mut context);
                }
                if let Some(last_ir) = self.pop_ir() {
                    let rt = context.get_type_mut(&result_type).unwrap();
                    rt.resolve(&last_ir.result_type());
                    self.push_ir(IR::new_set_local(last_ir.result_type().clone(), location.clone(), value_var_name, last_ir.clone()));
                    self.push_ir(IR::new_jump(last_ir.result_type().clone(), location.clone(), following_block_idx.into()));
                } else {
                    self.push_ir(IR::new_jump(result_type.clone(), location.clone(), following_block_idx.into()));
                }
                let positive = self.pop_block().unwrap();
                let positive_idx = context.push_block(positive);

                // Push the positive jump test.
                self.push_ir(IR::new_jump_if_true(
                    result_type.clone(),
                    location.clone(),
                    test.clone(),
                    positive_idx,
                ));

                // If there is an `else` clause then build a block concluding
                // with a jump to the following block and push a `jump_if_false`
                // to the block.
                if !negative.is_empty() {
                    self.push_block(context.unknown_type(location.clone()));
                    for node in negative {
                        self.build(node.clone(), &mut context);
                    }

                    if let Some(last_ir) = self.pop_ir() {
                        let rt = context.get_type(&result_type).unwrap();
                        if rt.target().unwrap() != last_ir.result_type() {
                            context.compile_error("Both branches of an if statement must result in the same type.", location.clone(), ErrorKind::InconsistentBranchTypes);
                        }

                        self.push_ir(IR::new_set_local(last_ir.result_type().clone(), location.clone(), value_var_name, last_ir.clone()));
                        self.push_ir(IR::new_jump(last_ir.result_type().clone(), location.clone(), following_block_idx.into()));
                    } else {
                        self.push_ir(IR::new_jump(result_type.clone(), location.clone(), following_block_idx.into()));
                    }

                    let negative = self.pop_block().unwrap();
                    let negative_idx = context.push_block(negative);

                    self.push_ir(IR::new_jump(
                        result_type,
                        location.clone(),
                        negative_idx,
                    ));
                }

                // Finally, push the following block onto the stack for the following instructions.
                self.push_block(context.unknown_type(location.clone()));

                if let Some(result_type) = self.env_get(value_var_name) {
                    self.push_ir(IR::new_get_local(result_type.clone(), location, value_var_name));
                }
            }
            NodeType::ImplDef => {
                let (ty, body) = node.impldef().unwrap();
                let ty_idx = context.constant_string(ty.value_ref().as_str());
                let location = context.location(&node.location());
                let tr = context.reference_type(ty_idx, location.clone());

                // the parser state should stop us from being in an
                // implementation whilst not inside a type definition.
                let ty = self.peek_ty().unwrap().clone();

                let im = context.declare_impl(tr, ty, location.clone());

                self.push_ty(im.clone());
                self.push_block(context.unknown_type(location.clone()));

                for node in body {
                    self.build(node.clone(), &mut context);
                }

                let block = self.pop_block().unwrap();
                assert!(block.is_empty());
                self.pop_ty();
            }
            NodeType::Integer => {
                let ty_idx = context.constant_string("Huia.Native.Integer");
                let location = context.location(&node.location());
                let ty = context.reference_type(ty_idx, location.clone());
                let value = *node.integer().unwrap().value_ref();
                self.push_ir(IR::new_constant(ty, location, Val::Integer(value)));
            }
            NodeType::Local => {
                let name = node.local().unwrap();
                let name = name.value_ref().as_str();
                let location = context.location(&node.location());
                let constant_name = context.constant_string(name);
                match self.env_get(constant_name) {
                    Some(result_type) => self.push_ir(IR::new_get_local(result_type.clone(), location, constant_name)),
                    None => {
                        // Add a compiler error.
                        let message = format!("Unknown variable {}", name);
                        context.compile_error(&message, location.clone(), ErrorKind::UnknownVariable);

                        // Push a value of unknown type to the IR so that we can try and continue.
                        let result_type = context.unknown_type(location.clone());
                        self.push_ir(IR::new_get_local(result_type, location, constant_name));
                    }
                }
            }
            NodeType::Map => {
                let ty_idx = context.constant_string("Huia.Native.Map");
                let location = context.location(&node.location());
                let ty = context.reference_type(ty_idx, location.clone());

                let elements = node
                    .map()
                    .unwrap()
                    .iter()
                    .map(|(key, value)| {
                        self.build(key.clone(), &mut context);
                        let key = self.pop_ir().unwrap();
                        self.build(value.clone(), &mut context);
                        let value = self.pop_ir().unwrap();
                        (key, value)
                    })
                    .collect();
                self.push_ir(IR::new_constant(ty, location, Val::Map(elements)));
            }
            NodeType::MethodCall => {
                let location = context.location(&node.location());
                let (callee, method_name, arguments) = node.method_call().unwrap();
                let method_name = context.constant_string(method_name.value_ref());

                self.build(callee.clone(), &mut context);
                let callee = self.pop_ir().unwrap();

                let arguments = arguments.iter().map(|node| {
                    self.build(node.clone(), &mut context);
                    self.pop_ir().unwrap()
                }).collect();
                let result_type = context.unknown_type(location.clone());

                self.push_ir(IR::new_method_call(result_type, location, callee, method_name, arguments))
            }
            NodeType::PropertyGet => {
                let location = context.location(&node.location());
                let name =
                    context.constant_string(node.property_get().unwrap().value_ref().as_str());
                let result_type = self.prop_ty(name, &context).unwrap();
                self.push_ir(IR::new_get_property(result_type, location, name));
            }
            NodeType::PropertySet => {
                let location = context.location(&node.location());
                let result_type = self.peek_ty_def(&context).unwrap().clone();
                let elements: Vec<(StringIdx, IR)> = node
                    .property_set()
                    .unwrap()
                    .iter()
                    .map(|(key, value)| {
                        let key = context.constant_string(key.value_ref().as_str());

                        self.build(value.clone(), &mut context);
                        let value = self.pop_ir().unwrap();
                        (key, value)
                    })
                    .collect();

                self.push_ir(IR::new_set_properties(result_type, location, elements))
            }
            NodeType::PrivateMethod => {
                let location = context.location(&node.location());
                let (ident, arguments, body) = node.private_method().unwrap();
                let method_name = context.constant_string(ident.value_ref().as_str());
                let modifier = Modifier::Private;
                self.build_method(&mut context, method_name, modifier, location, arguments, body);
            }
            NodeType::PublicMethod => {
                let location = context.location(&node.location());
                let (ident, arguments, body) = node.public_method().unwrap();
                let method_name = context.constant_string(ident.value_ref().as_str());
                let modifier = Modifier::Public;
                self.build_method(&mut context, method_name, modifier, location, arguments, body);
            }
            NodeType::PublicMethodSpec => {
                let location = context.location(&node.location());
                let (ident, arguments, return_type) = node.public_method_spec().unwrap();

                let method_name = context.constant_string(ident.value_ref().as_str());
                let parent_type = self.peek_ty().unwrap().clone();
                let arguments = self.build_arguments(&mut context, arguments);
                let return_type = if return_type.is_some() {
                    self.build_anonymous_trait(&mut context, &return_type.unwrap())
                } else {
                    let name = context.constant_string("Boolean");
                    context.reference_type(name, location.clone())
                };
                let method = Method::new_spec(method_name, arguments, return_type, parent_type, Modifier::Public, location);

                context.define_method(method);
            }
            NodeType::StaticMethod => {
                let location = context.location(&node.location());
                let (ident, arguments, body) = node.static_method().unwrap();
                let method_name = context.constant_string(ident.value_ref().as_str());
                let modifier = Modifier::Static;
                self.build_method(&mut context, method_name, modifier, location, arguments, body);
            }
            NodeType::StaticMethodSpec => {
                let location = context.location(&node.location());
                let (ident, arguments, return_type) = node.static_method_spec().unwrap();

                let method_name = context.constant_string(ident.value_ref().as_str());
                let return_type = if return_type.is_some() {
                    self.build_anonymous_trait(&mut context, &return_type.unwrap())
                } else {
                    let name = context.constant_string("Boolean");
                    context.reference_type(name, location.clone())
                };
                let parent_type = self.peek_ty().unwrap().clone();
                let arguments = self.build_arguments(&mut context, arguments);

                let method = Method::new_spec(method_name, arguments, return_type, parent_type, Modifier::Static, location);
                context.define_method(method);
            }
            NodeType::String => {
                let ty_idx = context.constant_string("Huia.Native.String");
                let location = context.location(&node.location());
                let ty = context.reference_type(ty_idx, location.clone());
                let idx = context.constant_string(node.string().unwrap().value_ref().as_str());
                self.push_ir(IR::new_constant(ty, location, Val::String(idx)));
            }
            NodeType::TraitDef => {
                let (ty, typespec, body) = node.traitdef().unwrap();
                let ty_idx = context.constant_string(ty.value_ref().as_str());
                let location = context.location(&node.location());

                let types = if typespec.is_some() {
                    typespec
                        .unwrap()
                        .value_ref()
                        .iter()
                        .map(|node| {
                            let name = context.constant_string(node.value_ref().as_str());
                            let location = context.location(&node.location());
                            context.reference_type(name, location)
                        })
                        .collect()
                } else {
                    Vec::new()
                };

                let ty = context.declare_trait(ty_idx, types, location.clone());
                self.push_ty(ty.clone());
                self.push_block(context.unknown_type(location));

                for node in body {
                    self.build(node.clone(), &mut context);
                }

                let block = self.pop_block().unwrap();
                assert!(block.is_empty());
                self.pop_ty();
            }
            NodeType::Ty => {
                let location = context.location(&node.location());
                let result_type_idx = context.constant_string("Huia.Native.Type");
                let result_type = context.reference_type(result_type_idx, location.clone());
                let ty_idx = context.constant_string(node.ty().unwrap().value_ref().as_str());
                let ty = context.reference_type(ty_idx, location.clone());
                self.push_ir(IR::new_type_reference(result_type, location, ty));
            }
            NodeType::TypeDef => {
                let (ty, properties, body) = node.typedef().unwrap();
                let ty_idx = context.constant_string(ty.value_ref().as_str());
                let location = context.location(&node.location());

                let properties = properties
                    .iter()
                    .map(|(key, value)| {
                        let key = context.constant_string(key.value_ref().as_str());
                        let ty = self.build_anonymous_trait(&mut context, value);
                        (key, ty)
                    })
                    .collect();

                let ty = context.declare_type(ty_idx, properties, location.clone());
                self.push_ty(ty.clone());
                self.push_block(context.unknown_type(location));
                for node in body {
                    self.build(node.clone(), &mut context);
                }
                let block = self.pop_block().unwrap();
                assert!(block.is_empty());
                self.pop_ty();
            }
            NodeType::Unary => {
                let location = context.location(&node.location());
                let (op, rhs) = node.unary().unwrap();
                self.build(rhs.clone(), &mut context);
                let rhs = self.pop_ir().unwrap();
                let result_type = rhs.result_type().clone();
                self.push_ir(IR::new_unary(
                    result_type,
                    location,
                    op.value_ref().clone(),
                    rhs,
                ));
            }

            // _ => unreachable!("Unexpected node type: {:?}", node.node_type()),
        }
    }

    fn build_anonymous_trait(&self, context: &mut Context, typespec: &TypeSpec) -> TyIdx {
        let location = context.location(&typespec.location());
        let types = typespec
            .value_ref()
            .iter()
            .map(|node| {
                let name = context.constant_string(node.value_ref().as_str());
                let location = context.location(&node.location());
                context.reference_type(name, location)
            })
            .collect();
        context.anonymous_trait(types, location)
    }

    fn build_arguments(
        &self,
        mut context: &mut Context,
        arguments: &[(Identifier, TypeSpec)],
    ) -> Vec<(StringIdx, TyIdx)> {
        arguments
            .iter()
            .map(|(name, type_spec)| {
                let name = context.constant_string(name.value_ref().as_str());
                let ty = self.build_anonymous_trait(&mut context, type_spec);
                (name, ty)
            })
            .collect()
    }

    fn build_clause(
        &mut self,
        mut context: &mut Context,
        arguments: &[(Identifier, TypeSpec)],
        body: &[Term],
    ) -> (Vec<(StringIdx, TyIdx)>, BlockIdx) {
        let arguments = self.build_arguments(&mut context, arguments);

        self.push_block(context.unknown_type(context.location(body[0].location())));
        let current_block_len = self.blocks.len();

        for (name, ty) in &arguments {
            self.env_set(*name, ty);
        }

        for node in body {
            self.build(node.clone(), &mut context);
        }

        // If there is at least one value in this block we know we
        // can at least return it.
        // If it has no returnable value then IDK what to do.
        if let Some(last_ir) = self.pop_ir() {
            if last_ir.is_terminator() {
                // If the IR is already a block terminator then just roll with it.
                self.push_ir(last_ir);
            } else {
                self.push_ir(IR::new_return(
                    last_ir.result_type().clone(),
                    last_ir.get_location().clone(),
                    last_ir,
                ));
            }
        }

        // It's entirely possible that this clauses's CFG contains more than one
        // block, so keep popping until we're back where we started.
        while self.blocks.len() > current_block_len {
            let block = self.pop_block().unwrap();
            context.push_block(block);
        }
        let clause_data = self.pop_block().unwrap();
        let idx = context.push_block(clause_data);
        (arguments, idx)
    }

    fn build_function_clause(
        &mut self,
        mut context: &mut Context,
        fun: &mut Function,
        arguments: &[(Identifier, TypeSpec)],
        body: &[Term],
        location: Loc,
    ) {
        let (arguments, block_idx) = self.build_clause(&mut context, arguments, body);
        let result_type = context.unknown_type(location.clone());
        fun.push_clause(arguments, block_idx, result_type);
    }

    fn build_method(
        &mut self,
        mut context: &mut Context,
        method_name: StringIdx,
        modifier: Modifier,
        location: Loc,
        arguments: &[(Identifier, TypeSpec)],
        body: &[Term],
    ) {
        let parent_type = self.peek_ty().unwrap().clone();

        let method_idx = match context.find_method(&parent_type, method_name, modifier) {
            Some(idx) => idx,
            None => {
                let return_type = context.unknown_type(location.clone());
                let method = Method::new(
                    method_name,
                    return_type,
                    parent_type.clone(),
                    modifier,
                    location.clone(),
                );
                context.define_method(method)
            }
        };

        self.build_method_clause(&mut context, &method_idx, arguments, body);
    }

    fn build_method_clause(
        &mut self,
        mut context: &mut Context,
        method_idx: &MethodIdx,
        arguments: &[(Identifier, TypeSpec)],
        body: &[Term],
    ) {
        let (arguments, block_idx) = self.build_clause(&mut context, arguments, body);
        let method = context.get_method(method_idx.clone()).unwrap();
        let result_type = context.unknown_type(method.location().clone());
        let method = context.get_method_mut(method_idx.clone()).unwrap();
        method.push_clause(arguments, block_idx, result_type);
    }

    fn env_set(&mut self, name: StringIdx, ty: &TyIdx) {
        self.peek_block_mut().unwrap().env_set(name, ty.clone());
    }

    fn env_get(&self, name: StringIdx) -> Option<&TyIdx> {
        for block in self.blocks.iter().rev() {
            if let Some(idx) = block.env_get(name) {
                return Some(idx);
            }
        }
        None
    }

    fn peek_block(&self) -> Option<&Block> {
        let len = self.blocks.len();
        if len > 0 {
            self.blocks.get(len - 1)
        } else {
            None
        }
    }

    fn peek_block_mut(&mut self) -> Option<&mut Block> {
        let len = self.blocks.len();
        self.blocks.get_mut(len - 1)
    }

    fn peek_ty(&self) -> Option<&TyIdx> {
        let len = self.types.len();
        self.types.get(len - 1)
    }

    /// Walk through the type stack and find a type that's a real type
    /// definition, not an implementation.
    fn peek_ty_def(&self, context: &Context) -> Option<&TyIdx> {
        for idx in self.types.iter().rev() {
            let ty = context.get_type(idx).unwrap();
            if ty.is_type() {
                return Some(idx);
            }
        }

        None
    }

    fn pop_block(&mut self) -> Option<Block> {
        self.blocks.pop()
    }

    pub fn pop_ir(&mut self) -> Option<IR> {
        self.peek_block_mut().unwrap().pop()
    }

    fn pop_ty(&mut self) -> Option<TyIdx> {
        self.types.pop()
    }

    fn prop_ty(&self, name: StringIdx, context: &Context) -> Option<TyIdx> {
        for ty_idx in self.types.iter().rev() {
            let ty = context.get_type(ty_idx).unwrap();
            let p = ty.get_prop_type(name);
            if p.is_some() {
                return Some(p.unwrap().clone());
            }
        }
        None
    }

    pub fn push_block(&mut self, result_type: TyIdx) {
        match self.peek_block() {
            Some(ref block) => {
                let env = block.env().clone();
                self.blocks.push(Block::new(env, result_type));
            }
            None => {
                let env = Env::default();
                self.blocks.push(Block::new(env, result_type));
            }
        }
    }

    fn push_ir(&mut self, ir: IR) {
        self.peek_block_mut().unwrap().push(ir);
    }

    fn push_ty(&mut self, idx: TyIdx) {
        self.types.push(idx);
    }
}

impl Default for Builder {
    fn default() -> Builder {
        let blocks = Vec::default();
        let types = Vec::default();
        Builder { blocks, types }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_atom() {
        let term = Term::input(":marty").unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_constant());
        assert!(context.find_type_by_name("Huia.Native.Atom").is_some());
    }

    #[test]
    fn test_boolean() {
        let term = Term::input("true").unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_constant());
        assert!(context.find_type_by_name("Huia.Native.Boolean").is_some());
    }

    #[test]
    fn test_float() {
        let term = Term::input("1.23").unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_constant());
        assert!(context.find_type_by_name("Huia.Native.Float").is_some());
    }

    #[test]
    fn test_string() {
        let term = Term::input(r#" "Marty McFly"  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_constant());
        assert!(context.find_type_by_name("Huia.Native.String").is_some());
    }

    #[test]
    fn test_type_reference() {
        let term = Term::input(r#" MartyMcFly  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_type_reference());
        assert!(context.find_type_by_name("MartyMcFly").is_some());
    }

    #[test]
    fn test_array() {
        let term = Term::input(r#" [1, 2, 3]  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_constant());
        assert!(context.find_type_by_name("Huia.Native.Array").is_some());
        assert!(context.find_type_by_name("Huia.Native.Integer").is_some());
    }

    #[test]
    fn test_map() {
        let term = Term::input(r#" { a: 2, :b => 3 }  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_constant());
        assert!(context.find_type_by_name("Huia.Native.Map").is_some());
        assert!(context.find_type_by_name("Huia.Native.Atom").is_some());
        assert!(context.find_type_by_name("Huia.Native.Integer").is_some());
    }

    #[test]
    fn test_if() {
        let term = Term::input(r#" if true do 123 end  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);

        // Pop the follower block off the block stack.
        builder.pop_block().unwrap();

        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_jump_if_true());
        let block = builder.pop_block().unwrap();
        assert!(block.is_empty());
    }

    #[test]
    fn test_if_else() {
        let term = Term::input(r#" if true do 123 else 456 end  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);

        // Pop the follower block off the block stack.
        builder.pop_block().unwrap();

        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_jump());
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_jump_if_true());
        builder.pop_block().unwrap();
    }

    #[test]
    fn test_infix() {
        let term = Term::input(r#" 1 + 2  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_infix());
        assert!(context.find_type_by_name("Huia.Native.Integer").is_some());
    }

    #[test]
    fn test_constructor() {
        let term = Term::input(r#" Delorean { speed: 88 }  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_constructor());
        assert!(context.find_type_by_name("Delorean").is_some());
        assert!(context.find_type_by_name("Huia.Native.Integer").is_some());
    }

    #[test]
    fn test_unary() {
        let term = Term::input(r#" +88  "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_unary());
        assert!(context.find_type_by_name("Huia.Native.Integer").is_some());
    }

    #[test]
    fn test_call() {
        let terms = Term::input(" let x = 1\n x(123)  ").unwrap();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        for term in terms {
            builder.build(term, &mut context);
        }
        let call = builder.pop_ir().unwrap();
        assert!(call.is_call());
        assert!(context.find_type_by_name("Huia.Native.Integer").is_some());
    }

    #[test]
    fn test_set_local() {
        let term = Term::input(r#" let x = 123 "#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_set_local());
        assert!(context.find_type_by_name("Huia.Native.Integer").is_some());
    }

    #[test]
    fn test_type_definition() {
        let term = Term::file("type Delorean(speed: Integer)").unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        assert!(context.find_type_by_name("Delorean").is_some());
        assert!(context.find_type_by_name("Integer").is_some());
    }

    #[test]
    fn test_trait_definition() {
        let term = Term::file("trait TimeMachine").unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        assert!(context.find_type_by_name("TimeMachine").is_some());
    }

    #[test]
    fn test_trait_implementation() {
        let term = Term::file(
            r#"
                type Delorean(speed: Integer) do
                    impl TimeMachine
                end
          "#,
        )
        .unwrap()[0]
            .clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        assert!(context.find_type_by_name("Delorean").is_some());
        assert!(context.find_type_by_name("TimeMachine").is_some());
    }

    #[test]
    fn test_anonymous_function() {
        let term = Term::input(r#"fn (speed: Integer) do "WAT" end"#).unwrap()[0].clone();
        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
        let ir = builder.pop_ir().unwrap();
        assert!(ir.is_function());

        let fun = context
            .get_function(ir.function().unwrap().clone())
            .unwrap();
        assert!(!fun.is_empty());
    }

    #[test]
    fn test_acceptance() {
        let term = Term::file(
            r#"
            type Delorean(speed: Integer, year: Integer, target_year: Integer) do
                defs init() do
                    Delorean { speed: 0, year: 1985, target_year: 1985 }
                end

                def current_speed() do
                    @speed
                end

                def accelerate() do
                    let speed = @speed + 1
                    if (speed == 88) do
                        @{ speed: speed, year: @target_year }
                    else
                        @{ speed: speed }
                    end
                end

                impl TimeMachine do
                    def current_year() do
                        @year
                    end

                    def set_target_year(year: Integer) do
                        @{ target_year: year }
                    end
                end
            end
        "#,
        )
        .unwrap()[0]
            .clone();

        let mut builder = Builder::default();
        let mut context = Context::test();
        builder.push_block(context.unknown_type(Loc::test()));
        builder.build(term, &mut context);
    }
}
