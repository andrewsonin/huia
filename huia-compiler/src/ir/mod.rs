pub mod builder;

use crate::block::BlockIdx;
use crate::context::Context;
use crate::function::FunctionIdx;
use crate::location::Location;
use crate::stable::StringIdx;
use crate::ty::{ResultType, TyIdx};
use huia_parser::ast::binary::Operator as BinOp;
use huia_parser::ast::unary::Operator as UnOp;

/// Describes the intermediate representation as converted from the Parser's AST.
#[derive(Debug, PartialEq, Clone)]
pub struct IR {
    location: Location,
    result_type: TyIdx,
    kind: IRKind,
}

impl IR {
    pub fn function(&self) -> Option<&FunctionIdx> {
        match self.kind {
            IRKind::FunctionRef(ref idx) => Some(idx),
            _ => None,
        }
    }

    pub fn get_call(&self) -> Option<(&IR, &Vec<IR>)> {
        match self.kind {
            IRKind::Call(ref ir, ref args) => Some((ir, args)),
            _ => None,
        }
    }

    pub fn get_constructor(&self) -> Option<&Vec<(StringIdx, IR)>> {
        match self.kind {
            IRKind::Constructor(ref properties) => Some(properties),
            _ => None,
        }
    }

    pub fn get_function(&self) -> Option<&FunctionIdx> {
        match self.kind {
            IRKind::FunctionRef(ref idx) => Some(idx),
            _ => None,
        }
    }

    pub fn get_infix(&self) -> Option<(&BinOp, &IR, &IR)> {
        match self.kind {
            IRKind::Infix(ref op, ref lhs, ref rhs) => Some((op, lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_jump(&self) -> Option<&BlockIdx> {
        match self.kind {
            IRKind::Jump(ref idx) => Some(idx),
            _ => None,
        }
    }

    pub fn get_jump_if_false(&self) -> Option<(&IR, &BlockIdx)> {
        match self.kind {
            IRKind::JumpIfFalse(ref ir, ref idx) => Some((ir, idx)),
            _ => None,
        }
    }

    pub fn get_jump_if_true(&self) -> Option<(&IR, &BlockIdx)> {
        match self.kind {
            IRKind::JumpIfTrue(ref ir, ref idx) => Some((ir, idx)),
            _ => None,
        }
    }

    pub fn get_kind(&self) -> &IRKind {
        &self.kind
    }

    pub fn get_local(&self) -> Option<&StringIdx> {
        match self.kind {
            IRKind::GetLocal(ref name) => Some(name),
            _ => None,
        }
    }

    pub fn get_location(&self) -> &Location {
        &self.location
    }

    pub fn get_method_call(&self) -> Option<(&IR, &StringIdx, &Vec<IR>)> {
        match self.kind {
            IRKind::MethodCall(ref rx, ref name, ref args) => Some((rx, name, args)),
            _ => None,
        }
    }

    pub fn get_property(&self) -> Option<&StringIdx> {
        match self.kind {
            IRKind::GetProperty(ref name) => Some(name),
            _ => None,
        }
    }

    pub fn get_return(&self) -> Option<&IR> {
        match self.kind {
            IRKind::Return(ref ir) => Some(ir),
            _ => None,
        }
    }

    pub fn get_set_local(&self) -> Option<(&StringIdx, &IR)> {
        match self.kind {
            IRKind::SetLocal(ref name, ref value) => Some((name, value)),
            _ => None,
        }
    }

    pub fn get_set_properties(&self) -> Option<&Vec<(StringIdx, IR)>> {
        match self.kind {
            IRKind::SetProperties(ref properties) => Some(properties),
            _ => None,
        }
    }

    pub fn get_target(&self) -> Option<&BlockIdx> {
        match self.kind {
            IRKind::Jump(ref idx) => Some(idx),
            IRKind::JumpIfFalse(_, ref idx) => Some(idx),
            IRKind::JumpIfTrue(_, ref idx) => Some(idx),
            _ => None,
        }
    }

    pub fn get_type_reference(&self) -> Option<&TyIdx> {
        match self.kind {
            IRKind::TypeReference(ref ty) => Some(ty),
            _ => None,
        }
    }

    pub fn get_unary(&self) -> Option<(&UnOp, &IR)> {
        match self.kind {
            IRKind::Unary(ref op, ref rhs) => Some((op, rhs)),
            _ => None,
        }
    }

    pub fn get_value(&self) -> Option<&Val> {
        match self.kind {
            IRKind::Constant(ref value) => Some(value),
            _ => None,
        }
    }

    /// Provided an "improver" function, possibly improve this node. Implies
    /// improvement of child nodes as required.
    ///
    /// Child nodes are visited *before* the receiving node to ensure that
    /// improvements can bubble up.
    pub fn improve<F: Fn(&mut IR, &mut Context) + Copy>(
        &mut self,
        mut context: &mut Context,
        improver: F,
    ) {
        match self.kind {
            IRKind::Call(ref mut fun, ref mut args) => {
                fun.improve(&mut context, improver);
                for arg in args.iter_mut() {
                    arg.improve(&mut context, improver);
                }
            }
            IRKind::Constant(ref mut value) => match value {
                Val::Array(ref mut elements) => {
                    for element in elements.iter_mut() {
                        element.improve(&mut context, improver);
                    }
                }
                Val::Map(ref mut elements) => {
                    for (ref mut key, ref mut value) in elements.iter_mut() {
                        key.improve(&mut context, improver);
                        value.improve(&mut context, improver);
                    }
                }
                _ => (),
            },
            IRKind::Constructor(ref mut properties) => {
                for (_key, ref mut value) in properties.iter_mut() {
                    value.improve(&mut context, improver);
                }
            }
            IRKind::FunctionRef(..) => (),
            IRKind::GetLocal(..) => (),
            IRKind::GetProperty(..) => (),
            IRKind::Infix(_, ref mut lhs, ref mut rhs) => {
                lhs.improve(&mut context, improver);
                rhs.improve(&mut context, improver);
            }
            IRKind::Jump(..) => (),
            IRKind::JumpIfFalse(ref mut test, ..) => {
                test.improve(&mut context, improver);
            }
            IRKind::JumpIfTrue(ref mut test, ..) => {
                test.improve(&mut context, improver);
            }
            IRKind::MethodCall(ref mut callee, _, ref mut arguments) => {
                callee.improve(&mut context, improver);
                for argument in arguments {
                    argument.improve(&mut context, improver);
                }
            }
            IRKind::Return(ref mut value) => {
                value.improve(&mut context, improver);
            }
            IRKind::SetLocal(_, ref mut rhs) => {
                rhs.improve(&mut context, improver);
            }
            IRKind::SetProperties(ref mut properties) => {
                for (_key, ref mut value) in properties.iter_mut() {
                    value.improve(&mut context, improver);
                }
            }
            IRKind::TypeReference(..) => (),
            IRKind::Unary(_, ref mut rhs) => {
                rhs.improve(&mut context, improver);
            }
        }
        improver(self, &mut context);
    }

    pub fn is_call(&self) -> bool {
        match self.kind {
            IRKind::Call(..) => true,
            _ => false,
        }
    }

    pub fn is_constant(&self) -> bool {
        match self.kind {
            IRKind::Constant(..) => true,
            _ => false,
        }
    }

    pub fn is_constant_float(&self) -> bool {
        match self.kind {
            IRKind::Constant(ref value) => value.is_float(),
            _ => false,
        }
    }

    pub fn is_constructor(&self) -> bool {
        match self.kind {
            IRKind::Constructor(..) => true,
            _ => false,
        }
    }

    pub fn is_function(&self) -> bool {
        match self.kind {
            IRKind::FunctionRef(..) => true,
            _ => false,
        }
    }

    pub fn is_get_local(&self) -> bool {
        match self.kind {
            IRKind::GetLocal(..) => true,
            _ => false,
        }
    }

    pub fn is_get_property(&self) -> bool {
        match self.kind {
            IRKind::GetProperty(..) => true,
            _ => false,
        }
    }

    pub fn is_infix(&self) -> bool {
        match self.kind {
            IRKind::Infix(..) => true,
            _ => false,
        }
    }

    pub fn is_jump(&self) -> bool {
        match self.kind {
            IRKind::Jump(..) => true,
            _ => false,
        }
    }

    pub fn is_jump_any(&self) -> bool {
        match self.kind {
            IRKind::Jump(..) => true,
            IRKind::JumpIfFalse(..) => true,
            IRKind::JumpIfTrue(..) => true,
            _ => false,
        }
    }

    pub fn is_jump_if_false(&self) -> bool {
        match self.kind {
            IRKind::JumpIfFalse(..) => true,
            _ => false,
        }
    }

    pub fn is_jump_if_true(&self) -> bool {
        match self.kind {
            IRKind::JumpIfTrue(..) => true,
            _ => false,
        }
    }

    pub fn is_method_call(&self) -> bool {
        match self.kind {
            IRKind::MethodCall(..) => true,
            _ => false,
        }
    }

    pub fn is_return(&self) -> bool {
        match self.kind {
            IRKind::Return(..) => true,
            _ => false,
        }
    }

    pub fn is_set_local(&self) -> bool {
        match self.kind {
            IRKind::SetLocal(..) => true,
            _ => false,
        }
    }

    pub fn is_set_properties(&self) -> bool {
        match self.kind {
            IRKind::SetProperties(..) => true,
            _ => false,
        }
    }

    pub fn is_terminator(&self) -> bool {
        match self.kind {
            IRKind::Jump(..) => true,
            IRKind::JumpIfFalse(..) => true,
            IRKind::JumpIfTrue(..) => true,
            IRKind::Return(..) => true,
            _ => false,
        }
    }

    pub fn is_type_reference(&self) -> bool {
        match self.kind {
            IRKind::TypeReference(..) => true,
            _ => false,
        }
    }

    pub fn is_unary(&self) -> bool {
        match self.kind {
            IRKind::Unary(..) => true,
            _ => false,
        }
    }

    pub fn new_call(
        result_type: TyIdx,
        location: Location,
        function: IR,
        arguments: Vec<IR>,
    ) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::Call(Box::new(function), arguments),
        }
    }

    pub fn new_constant(result_type: TyIdx, location: Location, value: Val) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::Constant(value),
        }
    }

    pub fn new_constructor(
        result_type: TyIdx,
        location: Location,
        properties: Vec<(StringIdx, IR)>,
    ) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::Constructor(properties),
        }
    }

    pub fn new_function_ref(result_type: TyIdx, location: Location, function: FunctionIdx) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::FunctionRef(function),
        }
    }

    pub fn new_get_local(result_type: TyIdx, location: Location, name: StringIdx) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::GetLocal(name),
        }
    }

    pub fn new_get_property(result_type: TyIdx, location: Location, name: StringIdx) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::GetProperty(name),
        }
    }

    pub fn new_infix(result_type: TyIdx, location: Location, op: BinOp, lhs: IR, rhs: IR) -> IR {
        let lhs = Box::new(lhs);
        let rhs = Box::new(rhs);
        IR {
            location,
            result_type,
            kind: IRKind::Infix(op, lhs, rhs),
        }
    }

    pub fn new_jump(result_type: TyIdx, location: Location, target: BlockIdx) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::Jump(target),
        }
    }

    pub fn new_jump_if_false(
        result_type: TyIdx,
        location: Location,
        test: IR,
        target: BlockIdx,
    ) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::JumpIfFalse(Box::new(test), target),
        }
    }

    pub fn new_jump_if_true(
        result_type: TyIdx,
        location: Location,
        test: IR,
        target: BlockIdx,
    ) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::JumpIfTrue(Box::new(test), target),
        }
    }

    pub fn new_method_call(
        result_type: TyIdx,
        location: Location,
        callee: IR,
        method_name: StringIdx,
        arguments: Vec<IR>,
    ) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::MethodCall(Box::new(callee), method_name, arguments),
        }
    }

    pub fn new_return(result_type: TyIdx, location: Location, value: IR) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::Return(Box::new(value)),
        }
    }

    pub fn new_set_local(result_type: TyIdx, location: Location, name: StringIdx, value: IR) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::SetLocal(name, Box::new(value)),
        }
    }

    pub fn new_set_properties(
        result_type: TyIdx,
        location: Location,
        properties: Vec<(StringIdx, IR)>,
    ) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::SetProperties(properties),
        }
    }

    pub fn new_type_reference(result_type: TyIdx, location: Location, ty: TyIdx) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::TypeReference(ty),
        }
    }

    pub fn new_unary(result_type: TyIdx, location: Location, op: UnOp, rhs: IR) -> IR {
        IR {
            location,
            result_type,
            kind: IRKind::Unary(op, Box::new(rhs)),
        }
    }

    pub fn set_kind(&mut self, kind: IRKind) {
        self.kind = kind;
    }

    pub fn set_result_type(&mut self, ty: TyIdx) {
        self.result_type = ty;
    }
}

impl ResultType for IR {
    fn result_type(&self) -> &TyIdx {
        &self.result_type
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum IRKind {
    /// Call the provided callee with arguments.
    Call(Box<IR>, Vec<IR>),
    /// A constant value of the specified type.
    Constant(Val),
    /// Construct an instance of type with the provided properties.
    Constructor(Vec<(StringIdx, IR)>),
    /// Reference an anonymous function from the context.
    FunctionRef(FunctionIdx),
    /// Reference a local variable.
    GetLocal(StringIdx),
    /// Refernce an instance property.
    GetProperty(StringIdx),
    /// Unconditional jump.
    Jump(BlockIdx),
    /// Jump if IR value is false.
    JumpIfFalse(Box<IR>, BlockIdx),
    /// Jump if IR value is true.
    JumpIfTrue(Box<IR>, BlockIdx),
    /// A binary operation with LHS and RHS.
    Infix(BinOp, Box<IR>, Box<IR>),
    /// Call the named method on the receiver.
    MethodCall(Box<IR>, StringIdx, Vec<IR>),
    /// Return a value from the block to the caller.
    Return(Box<IR>),
    /// Take a value and set it as a local variable.
    SetLocal(StringIdx, Box<IR>),
    /// Set properties on an instance, returning the modified instance.
    SetProperties(Vec<(StringIdx, IR)>),
    /// A direct type reference.
    TypeReference(TyIdx),
    /// A unary operation on a value.
    Unary(UnOp, Box<IR>),
}

/// A constant value.
#[derive(Debug, PartialEq, Clone)]
pub enum Val {
    Array(Vec<IR>),
    Atom(StringIdx),
    Boolean(bool),
    Float(f64),
    Integer(i64),
    Map(Vec<(IR, IR)>),
    String(StringIdx),
}

impl Val {
    pub fn array(&self) -> Option<Vec<&IR>> {
        match self {
            Val::Array(ref elements) => Some(elements.iter().collect()),
            _ => None,
        }
    }

    pub fn atom(&self) -> Option<&StringIdx> {
        match self {
            Val::Atom(ref atom) => Some(atom),
            _ => None,
        }
    }

    pub fn boolean(&self) -> Option<bool> {
        match self {
            Val::Boolean(ref value) => Some(*value),
            _ => None,
        }
    }

    pub fn float(&self) -> Option<f64> {
        match self {
            Val::Float(ref value) => Some(*value),
            _ => None,
        }
    }

    pub fn integer(&self) -> Option<i64> {
        match self {
            Val::Integer(ref value) => Some(*value),
            _ => None,
        }
    }

    pub fn string(&self) -> Option<&StringIdx> {
        match self {
            Val::String(ref idx) => Some(idx),
            _ => None,
        }
    }

    pub fn is_array(&self) -> bool {
        match self {
            Val::Array(..) => true,
            _ => false,
        }
    }

    pub fn is_atom(&self) -> bool {
        match self {
            Val::Atom(..) => true,
            _ => false,
        }
    }

    pub fn is_boolean(&self) -> bool {
        match self {
            Val::Boolean(..) => true,
            _ => false,
        }
    }

    pub fn is_float(&self) -> bool {
        match self {
            Val::Float(..) => true,
            _ => false,
        }
    }

    pub fn is_integer(&self) -> bool {
        match self {
            Val::Integer(..) => true,
            _ => false,
        }
    }

    pub fn is_map(&self) -> bool {
        match self {
            Val::Map(..) => true,
            _ => false,
        }
    }

    pub fn is_string(&self) -> bool {
        match self {
            Val::String(..) => true,
            _ => false,
        }
    }

    pub fn map(&self) -> Option<Vec<&(IR, IR)>> {
        match self {
            Val::Map(ref elements) => Some(elements.iter().collect()),
            _ => None,
        }
    }
}
