use crate::block::BlockIdx;
use crate::clause::{Clause, ClauseIdx, Clauseable};
use crate::location::{Locatable, Location};
use crate::stable::StringIdx;
use crate::ty::{ResultType, TyIdx};
use std::collections::BTreeMap;

#[derive(Debug, Clone)]
pub struct Method {
    clauses: Vec<Clause>,
    modifier: Modifier,
    location: Location,
    name: StringIdx,
    return_type: TyIdx,
    signature: BTreeMap<StringIdx, TyIdx>,
    parent_type: TyIdx,
}

impl Method {
    pub fn is_public(&self) -> bool {
        match self.modifier {
            Modifier::Public => true,
            _ => false,
        }
    }

    pub fn is_private(&self) -> bool {
        match self.modifier {
            Modifier::Private => true,
            _ => false,
        }
    }

    pub fn is_static(&self) -> bool {
        match self.modifier {
            Modifier::Static => true,
            _ => false,
        }
    }

    pub fn modifier(&self) -> Modifier {
        self.modifier
    }

    pub fn name(&self) -> &StringIdx {
        &self.name
    }

    pub fn new(
        name: StringIdx,
        return_type: TyIdx,
        parent_type: TyIdx,
        modifier: Modifier,
        location: Location,
    ) -> Method {
        Method {
            clauses: Vec::default(),
            modifier,
            location,
            name,
            return_type,
            signature: BTreeMap::default(),
            parent_type,
        }
    }

    pub fn new_spec(
        name: StringIdx,
        arguments: Vec<(StringIdx, TyIdx)>,
        return_type: TyIdx,
        parent_type: TyIdx,
        modifier: Modifier,
        location: Location,
    ) -> Method {
        let signature = arguments.iter().fold(BTreeMap::new(), |mut args, (k, v)| {
            args.insert(k.clone(), v.clone());
            args
        });
        Method {
            clauses: Vec::default(),
            modifier,
            location,
            name,
            return_type,
            signature,
            parent_type,
        }
    }

    pub fn parent_type(&self) -> &TyIdx {
        &self.parent_type
    }

    pub fn return_type(&self) -> &TyIdx {
        &self.return_type
    }
}

impl Clauseable for Method {
    fn clauses(&self) -> Vec<&Clause> {
        self.clauses.iter().collect()
    }

    fn has_clauses(&self) -> bool {
        !self.clauses.is_empty()
    }

    fn push_clause(
        &mut self,
        arguments: Vec<(StringIdx, TyIdx)>,
        block: BlockIdx,
        result_type: TyIdx,
    ) -> ClauseIdx {
        let idx = self.clauses.len();
        self.clauses
            .push(Clause::new(arguments, block, result_type));
        idx.into()
    }
}

impl Clauseable for &mut Method {
    fn clauses(&self) -> Vec<&Clause> {
        self.clauses.iter().collect()
    }

    fn has_clauses(&self) -> bool {
        !self.clauses.is_empty()
    }

    fn push_clause(
        &mut self,
        arguments: Vec<(StringIdx, TyIdx)>,
        block: BlockIdx,
        result_type: TyIdx,
    ) -> ClauseIdx {
        let idx = self.clauses.len();
        self.clauses
            .push(Clause::new(arguments, block, result_type));
        idx.into()
    }
}

impl Locatable for Method {
    fn location(&self) -> &Location {
        &self.location
    }
}

impl Locatable for &Method {
    fn location(&self) -> &Location {
        &self.location
    }
}

impl Locatable for &mut Method {
    fn location(&self) -> &Location {
        &self.location
    }
}

impl ResultType for &mut Method {
    fn result_type(&self) -> &TyIdx {
        &self.return_type
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Modifier {
    Public,
    Private,
    Static,
}

#[derive(Debug, Clone, PartialEq)]
pub struct MethodIdx(usize);

impl From<usize> for MethodIdx {
    fn from(i: usize) -> MethodIdx {
        MethodIdx(i)
    }
}

impl From<MethodIdx> for usize {
    fn from(i: MethodIdx) -> usize {
        i.0
    }
}

impl From<&MethodIdx> for usize {
    fn from(i: &MethodIdx) -> usize {
        i.0
    }
}
