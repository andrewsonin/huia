use crate::stable::StringIdx;
use crate::ty::TyIdx;
use std::collections::BTreeMap;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Env(BTreeMap<StringIdx, Vec<TyIdx>>);

impl Env {
    pub fn set(&mut self, name: StringIdx, ty: TyIdx) {
        match self.0.get_mut(&name) {
            Some(ref mut types) => {
                types.push(ty);
            }
            None => {
                self.0.insert(name, vec![ty]);
            }
        }
    }

    pub fn get(&self, name: StringIdx) -> Option<&TyIdx> {
        match self.0.get(&name) {
            Some(ref types) => {
                let len = types.len();
                types.get(len - 1)
            }
            None => None,
        }
    }
}
