//! Implements an IR improver which folds infix operations on numeric constants
//! at compile time.

use crate::context::Context;
use crate::ir::{IRKind, Val, IR};
use huia_parser::ast::binary::Operator as BinOp;
use std::f64;

/// Perform constant folding on the provided node, or not.
pub fn pass(ir: &mut IR, context: &mut Context) {
    if ir.is_infix() {
        let (op, lhs, rhs) = ir.get_infix().unwrap();
        if !(lhs.is_constant() && rhs.is_constant()) {
            return;
        }

        let lhs = lhs.get_value().unwrap();
        let rhs = rhs.get_value().unwrap();

        if lhs.is_integer() && rhs.is_integer() {
            let lhs = lhs.integer().unwrap();
            let rhs = rhs.integer().unwrap();
            let value = perform_i64_infix_fold(&op, lhs, rhs);
            if let Some(value) = value {
                ir.set_kind(IRKind::Constant(Val::Integer(value)));
                let idx = context.constant_string("Huia.Native.Integer");
                ir.set_result_type(context.reference_type(idx, ir.get_location().clone()));
            }
        } else if lhs.is_float() && rhs.is_float() {
            let lhs = lhs.float().unwrap();
            let rhs = rhs.float().unwrap();
            let value = perform_f64_infix_fold(&op, lhs, rhs);
            if let Some(value) = value {
                ir.set_kind(IRKind::Constant(Val::Float(value)));
                let idx = context.constant_string("Huia.Native.Float");
                ir.set_result_type(context.reference_type(idx, ir.get_location().clone()));
            }
        }
    }
}

fn perform_i64_infix_fold(op: &BinOp, lhs: i64, rhs: i64) -> Option<i64> {
    match op {
        BinOp::BitwiseAnd => Some(lhs & rhs),
        BinOp::BitwiseOr => Some(lhs | rhs),
        BinOp::BitwiseXor => Some(lhs ^ rhs),
        BinOp::Divide => Some(lhs / rhs),
        BinOp::Exponent => Some(lhs.pow(rhs as u32)), // FIXME: Potentially lossy conversion.
        BinOp::Minus => Some(lhs - rhs),
        BinOp::Modulus => Some(lhs % rhs),
        BinOp::Multiply => Some(lhs * rhs),
        BinOp::Plus => Some(lhs + rhs),
        BinOp::ShiftLeft => Some(lhs << rhs),
        BinOp::ShiftRight => Some(lhs >> rhs),
        _ => None,
    }
}

fn perform_f64_infix_fold(op: &BinOp, lhs: f64, rhs: f64) -> Option<f64> {
    match op {
        BinOp::Divide => Some(lhs / rhs),
        BinOp::Minus => Some(lhs - rhs),
        BinOp::Modulus => Some(lhs % rhs),
        BinOp::Multiply => Some(lhs * rhs),
        BinOp::Plus => Some(lhs + rhs),
        _ => None,
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::ir::builder::Builder;
    use crate::location::Location;
    use huia_parser::ast::Term;

    #[test]
    fn integer_infix_folding() {
        let term = Term::input("3 * 2 + 13 / 2").unwrap()[0].clone();
        let mut context = Context::test();
        let mut builder = Builder::default();
        builder.push_block(context.unknown_type(Location::test()));

        builder.build(term, &mut context);
        let mut ir = builder.pop_ir().unwrap();
        ir.improve(&mut context, &pass);

        assert!(ir.is_constant());
        assert_eq!(ir.get_value().unwrap().integer().unwrap(), 12);
    }

    #[test]
    fn float_infix_folding() {
        let term = Term::input("1.5 * 2.0 / 3.0").unwrap()[0].clone();
        let mut context = Context::test();
        let mut builder = Builder::default();
        builder.push_block(context.unknown_type(Location::test()));

        builder.build(term, &mut context);
        let mut ir = builder.pop_ir().unwrap();
        ir.improve(&mut context, &pass);

        assert!(ir.is_constant());
        assert!(ir.get_value().unwrap().float().unwrap() - 1.0 < f64::EPSILON);
    }
}
