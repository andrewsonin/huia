use crate::clause::Clauseable;
use crate::context::Context;
use crate::location::Locatable;
use crate::ty::ResultType;

pub fn pass<T: Clauseable + Locatable + ResultType>(fun: &mut T, context: &mut Context) {
    for clause in fun.clauses() {
        let block_idx = clause.body();
        let ty_idx = clause.result_type();
        let result_type = context.get_block(block_idx).unwrap().result_type().clone();
        let ty = context.get_type_mut(ty_idx).unwrap();
        ty.resolve(&result_type);
    }
}
