use crate::context::Context;
use crate::error::ErrorKind;

pub fn pass(context: &mut Context) {
    let mut unresolved_types = Vec::default();
    let mut inference_failures = Vec::default();
    let all_types = context.types();
    for ty in all_types {
        if ty.is_unresolved() {
            if ty.name().is_some() {
                let message = format!(
                    "Unable to resolve type {}",
                    context.get_string(ty.name().unwrap()).unwrap()
                );
                let location = ty.get_location().unwrap().clone();
                unresolved_types.push((message, location));
            } else {
                let message = "Unable to infer type";
                let location = ty.get_location().unwrap().clone();
                inference_failures.push((message, location));
            }
        }
    }
    for (message, location) in unresolved_types {
        context.compile_error(&message, location, ErrorKind::UnknownType);
    }
    for (message, location) in inference_failures {
        context.compile_error(message, location, ErrorKind::TypeInferenceFailure);
    }
}
