//! Implements an IR improver which folds infix operations on numeric constants
//! at compile time.

use crate::context::Context;
use crate::ir::{IRKind, Val, IR};
use huia_parser::ast::unary::Operator as UnOp;

/// Perform constant folding on the provided node, or not.
pub fn pass(ir: &mut IR, context: &mut Context) {
    if ir.is_unary() {
        let (op, rhs) = ir.get_unary().unwrap();
        if !rhs.is_constant() {
            return;
        }
        let rhs = rhs.get_value().unwrap();

        match op {
            UnOp::LogicalNot => {
                if rhs.is_boolean() {
                    // if the RHS is a boolean constant then fold it.
                    let value = rhs.boolean().unwrap();
                    ir.set_kind(IRKind::Constant(Val::Boolean(!value)));
                } else {
                    // otherwise just return false for all other constant values.
                    ir.set_kind(IRKind::Constant(Val::Boolean(false)));
                }
                let tyname = context.constant_string("Huia.Native.Boolean");
                let ty = context.reference_type(tyname, ir.get_location().clone());
                ir.set_result_type(ty)
            }
            UnOp::Minus => {
                if rhs.is_integer() {
                    let value = rhs.integer().unwrap();
                    ir.set_kind(IRKind::Constant(Val::Integer(-value)));
                } else if rhs.is_float() {
                    let value = rhs.float().unwrap();
                    ir.set_kind(IRKind::Constant(Val::Float(-value)));
                }
            }
            UnOp::Plus => {
                if rhs.is_integer() {
                    let value = rhs.integer().unwrap();
                    ir.set_kind(IRKind::Constant(Val::Integer(value)));
                } else if rhs.is_float() {
                    let value = rhs.float().unwrap();
                    ir.set_kind(IRKind::Constant(Val::Float(value)));
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::ir::builder::Builder;
    use crate::location::Location;
    use crate::ty::ResultType;
    use huia_parser::ast::Term;

    #[test]
    fn integer_unary_folding() {
        let term = Term::input("!3").unwrap()[0].clone();
        let mut context = Context::test();
        let mut builder = Builder::default();
        builder.push_block(context.unknown_type(Location::test()));

        builder.build(term, &mut context);
        let mut ir = builder.pop_ir().unwrap();
        ir.improve(&mut context, &pass);

        assert!(ir.is_constant());
        assert!(!ir.get_value().unwrap().boolean().unwrap());

        let ty = context.get_type(&ir.result_type()).unwrap();
        assert!(ty.is_native_boolean());
    }
}
