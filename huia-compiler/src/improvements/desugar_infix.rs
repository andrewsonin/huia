//! Implements an IR improver which converts infix operations into trait-related
//! method calls.

use crate::context::Context;
use crate::ir::{IRKind, IR};
use huia_parser::ast::binary::Operator as BinOp;

/// Desugar infix operations into method calls.
pub fn pass(ir: &mut IR, context: &mut Context) {
    if let Some((op, lhs, rhs)) = ir.get_infix() {
        let method_name = match op {
            BinOp::BitwiseAnd => "bitwise_and",
            BinOp::BitwiseOr => "bitwise_or",
            BinOp::BitwiseXor => "bitwise_xor",
            BinOp::Divide => "divide_by",
            BinOp::Equal => "equal_to?",
            BinOp::Exponent => "exponent",
            BinOp::GreaterThan => "greater_than?",
            BinOp::GreaterThanOrEqual => "greater_than_or_equal_to?",
            BinOp::LessThan => "less_than?",
            BinOp::LessThanOrEqual => "less_than_or_equal_to?",
            BinOp::LogicalAnd => "logical_and?",
            BinOp::LogicalOr => "logical_or?",
            BinOp::Minus => "subtract",
            BinOp::Modulus => "modulo",
            BinOp::Multiply => "multiply_by",
            BinOp::NotEqual => "not_equal_to?",
            BinOp::Plus => "add",
            BinOp::ShiftLeft => "shift_left",
            BinOp::ShiftRight => "shift_right",
        };
        let method_name = context.constant_string(method_name);
        ir.set_kind(IRKind::MethodCall(
            Box::new(lhs.clone()),
            method_name,
            vec![rhs.clone()],
        ));
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::ir::builder::Builder;
    use crate::location::Location;
    use huia_parser::ast::Term;

    #[test]
    fn works() {
        let terms = Term::input("3 * 2").unwrap();
        let mut context = Context::test();
        let mut builder = Builder::default();
        builder.push_block(context.unknown_type(Location::test()));

        for term in terms {
            builder.build(term, &mut context);
        }

        let mut ir = builder.pop_ir().unwrap();

        assert!(ir.is_infix());

        ir.improve(&mut context, &pass);

        assert!(ir.is_method_call());
    }
}
