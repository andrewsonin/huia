use crate::block::BlockIdx;
use crate::clause::Clauseable;
use crate::context::Context;
use crate::ty::ResultType;

pub fn pass<T: Clauseable>(fun: &mut T, mut context: &mut Context) {
    for clause in fun.clauses() {
        let block_idx = clause.body();
        set_block_result_type(block_idx, &mut context);
    }
}

fn set_block_result_type(block_idx: &BlockIdx, mut context: &mut Context) {
    let block = context.get_block(block_idx).unwrap().clone();
    if block.is_empty() {
        return;
    }

    for ir in block.ir_ref() {
        if let Some(target_idx) = ir.get_target() {
            set_block_result_type(target_idx, &mut context);
        }
    }

    if let Some(last_ir) = block.last_ir() {
        if last_ir.is_terminator() {
            let block = context.get_block(block_idx).unwrap().clone();
            let block_ty_idx = block.result_type();
            let block_ty = context.get_type_mut(block_ty_idx).unwrap();
            block_ty.resolve(&last_ir.result_type());
        }
    }
}
