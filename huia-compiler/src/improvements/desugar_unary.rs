//! Implements an IR improver which converts unary operations into trait-related
//! method calls.

use crate::context::Context;
use crate::ir::{IRKind, IR};
use huia_parser::ast::unary::Operator as UnOp;

/// Desugar unary operations into method calls.
pub fn pass(ir: &mut IR, context: &mut Context) {
    if ir.is_unary() {
        let (op, rhs) = ir.get_unary().unwrap();
        let method_name = match op {
            UnOp::LogicalNot => "logical_not",
            UnOp::Minus => "unary_minus",
            UnOp::Plus => "unary_plus",
        };
        let method_name = context.constant_string(method_name);
        ir.set_kind(IRKind::MethodCall(
            Box::new(rhs.clone()),
            method_name,
            Vec::default(),
        ));
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::ir::builder::Builder;
    use crate::location::Location;
    use huia_parser::ast::Term;

    #[test]
    fn works() {
        let terms = Term::input("!3").unwrap();
        let mut context = Context::test();
        let mut builder = Builder::default();
        builder.push_block(context.unknown_type(Location::test()));

        for term in terms {
            builder.build(term, &mut context);
        }

        let mut ir = builder.pop_ir().unwrap();

        assert!(ir.is_unary());

        ir.improve(&mut context, &pass);

        assert!(ir.is_method_call());
    }
}
