---
title: Huia
layout: index-page
---

# Design goals:

* Fast execution on native systems and browser via WebAssembly.
* Weighted reference counting instead of GC.
* Actor-based concurrency.
* Immutible objects.
* Trait-based polymorphism

# Status

Development of the open source Huia compiler is underway and is available on [GitLab](https://gitlab.com/jimsy/huia)
