use crate::ast::{Location, Value};
use crate::grammar::Rule;
use crate::input_location::InputLocation;
use pest::iterators::Pair;

#[derive(Debug, Clone, PartialEq)]
pub struct Boolean {
    value: bool,
    location: InputLocation,
}

impl Value for Boolean {
    type Item = bool;

    fn value(self) -> Self::Item {
        self.value
    }

    fn value_ref(&self) -> &Self::Item {
        &self.value
    }
}

impl Location for Boolean {
    fn location(&self) -> &InputLocation {
        &self.location
    }
}

impl<'a> From<Pair<'a, Rule>> for Boolean {
    fn from(pair: Pair<'a, Rule>) -> Self {
        match pair.as_rule() {
            Rule::boolean => {
                let inner = pair.into_inner().next().unwrap();

                match inner.as_rule() {
                    Rule::boolean_true => Boolean {
                        value: true,
                        location: InputLocation::from(inner.as_span()),
                    },
                    Rule::boolean_false => Boolean {
                        value: false,
                        location: InputLocation::from(inner.as_span()),
                    },
                    _ => unreachable!("Unexpected {:?} token inside boolean", inner),
                }
            }
            _ => unreachable!("Expected pair to be an Boolean"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::grammar::Grammar;
    use pest::Parser;

    #[test]
    fn test_true() {
        let pair = Grammar::parse(Rule::boolean, "true")
            .unwrap()
            .next()
            .unwrap();
        let boolean = Boolean::from(pair);
        assert_eq!(boolean.value(), true);
    }

    #[test]
    fn test_false() {
        let pair = Grammar::parse(Rule::boolean, "false")
            .unwrap()
            .next()
            .unwrap();
        let boolean = Boolean::from(pair);
        assert_eq!(boolean.value(), false);
    }
}
