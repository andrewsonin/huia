use crate::ast::{Location, Value};
use crate::grammar::Rule;
use crate::input_location::InputLocation;
use pest::iterators::Pair;

#[derive(Debug, Clone, PartialEq)]
pub struct Float {
    value: f64,
    location: InputLocation,
}

impl Value for Float {
    type Item = f64;

    fn value(self) -> Self::Item {
        self.value
    }

    fn value_ref(&self) -> &Self::Item {
        &self.value
    }
}

impl Location for Float {
    fn location(&self) -> &InputLocation {
        &self.location
    }
}

impl<'a> From<Pair<'a, Rule>> for Float {
    fn from(pair: Pair<'a, Rule>) -> Self {
        match pair.as_rule() {
            Rule::float => {
                let value = pair.clone().as_span().as_str().parse().unwrap();
                Float {
                    value,
                    location: InputLocation::from(pair.as_span()),
                }
            }
            _ => unreachable!("Expected pair to be an Float"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::grammar::Grammar;
    use pest::Parser;

    #[test]
    fn it_parses() {
        let pair = Grammar::parse(Rule::float, "123.456")
            .unwrap()
            .next()
            .unwrap();
        let float = Float::from(pair);
        assert!(float.value() - 123.456 < std::f64::EPSILON);
    }
}
