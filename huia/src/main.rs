use clap::{App, Arg, SubCommand};
use huia_compiler::compile_file;
use std::process::exit;

fn main() {
    let matches = App::new("Huia")
        .version("0.1.0")
        .author("James Harton <james@automat.nz>")
        .about("The Huia Programming Language")
        .subcommand(
            SubCommand::with_name("compile")
                .about("Compiles a Huia project")
                .arg(
                    Arg::with_name("INPUT")
                        .help("The path to the file to compile")
                        .required(true)
                        .takes_value(true),
                ),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("compile") {
        let path = matches.value_of("INPUT").unwrap();
        let context = compile_file(path);
        println!("{}", context);
        exit(0);
    }

    eprintln!("{}", matches.usage());
    exit(1);
}
